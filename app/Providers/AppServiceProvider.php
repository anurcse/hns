<?php

namespace App\Providers;

use App\Company;
use App\Customer;
use App\Item;
use App\Photo;
use App\Sales;
use App\SubCategorie;
use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Categorie;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        $categorie      = Categorie::where('cat_status','=',1)->get();

        $subcategoriess   = SubCategorie::where('subcat_status','=',1)->get();

        $company        = Company::where('company_status','=',1)->get();

        $photo = Photo::all();



        $customer = Customer::pluck('id')->last();
        $customer_last = count($customer) > 0 ? $customer + 1 : 1;





        $users = User::all();



        $items = Item::where('salse','=',0)->get();






        $items2 = Item::all();

//        $items3 = Item::where('salse','=',1 or 1)->get();

        $AvailableItem = count($items);




        $allCustomer = Customer::all();
        $last = 0;

        foreach ($allCustomer as $last){
            @$last->id;
        }


        $customer_last = @$last->id + 1;


        $todaysales = Sales::where('salesdate','=',$date = date('Y:m:d'))->get();

        foreach ($todaysales as $sumtodaysales){
            @$sumtodaysales +=$sumtodaysales->sales_price;
        }



        $allSalse = Sales::all();




//        $items = Item::pluck('id')->last();
//
//        $itesm_last = count($items) > 0 ? $items + 1 : 1;

        foreach ($allSalse as $tsales){

            @$tsales += $tsales->sales_price;
        }



        $userCount = count($users);



        View::share([
            'categorie'     => $categorie,
            'subcategoriess'  => $subcategoriess,
            'company'       => $company,
            'users'    =>$users,
            'items' =>$items,
            'items2' =>$items2,
//            'items3' =>$items3,

            //'customer_last'=>$customer_last,
            'customer_last'=>$customer_last,
            'allCustomer' =>$allCustomer,
            'allSalse'=>$allSalse,
            'photo' => $photo,
            'userCount' =>@$userCount,
            'AvailableItem'=>@$AvailableItem,
            'tsales' =>@$tsales +0,
            'sumtodaysales' =>@$sumtodaysales +0

        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
