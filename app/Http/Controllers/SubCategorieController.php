<?php

namespace App\Http\Controllers;

use App\Item;
use App\SubCategorie;
use Illuminate\Http\Request;

class SubCategorieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = Categorie::where('cat_status','=',1)->get();;
        $data = SubCategorie::all()->sortByDesc('id');
        return view('subcategory.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('subcategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[
            'cat_id' => 'required|max:255',
            'subcat_name' => 'required|max:255',
            'subcat_status'=>'required'
        ]);

        $data = new SubCategorie();

        $data->cat_id = $request->cat_id;
        $data->subcat_name = $request->subcat_name;
        $data->subcat_status = $request->subcat_status;


        if ($data->save()) {

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategorie = SubCategorie::where('id','=',$id)->get();
        return view('subcategory.edit')->with('subcategorie',$subcategorie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $updatedata = SubCategorie::find($request->id);

        $this->validate($request,[
            'cat_id' => 'required',
            'subcat_name' => 'required|max:255',
            'subcat_status'=>'required'
        ]);



        $updatedata->cat_id = $request->cat_id;
        $updatedata->subcat_name = $request->subcat_name;
        $updatedata->subcat_status = $request->subcat_status;


        if ($updatedata->save()) {

            $notification = array(
                'message' => 'Update Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Update Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //dd($request->id);

        //$deletes = Item::where('subcategorie','=',$request->id)->delete();

        // dd($deletes);



        if ($deletes = SubCategorie::find($request->id)->delete()) {

            $notification = array(
                'message' => 'Successfully Delete !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'This category have Item!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}