<?php

namespace App\Http\Controllers;

use App\Officecontact;
use Illuminate\Http\Request;

class OfficecontactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allContact = Officecontact::all()->sortByDesc('id');
        return view('officecontacts.index')->with('data',$allContact);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('officecontacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $addContact = new Officecontact();

        $addContact->name =$request->name;
        $addContact->address =$request->address;
        $addContact->contact_person =$request->contact_person;
        $addContact->phone1 =$request->phone1;
        $addContact->phone2 =$request->phone2;
        $addContact->email =$request->email;
        $addContact->description =$request->description;
        $addContact->company_id =$request->company_id;
        $addContact->status =$request->status;

        if ($addContact->save()) {

            $notification = array(
                'message' => 'Save Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Save Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userData =  Officecontact::where('id','=',$id)->get();
        return view('officecontacts.view')->with('data',$userData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $getContact = Officecontact::where('id','=',$id)->get();

        return view('officecontacts.edit')->with('data',$getContact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $updateOffice = Officecontact::find($request->id);

        $updateOffice->name =$request->name;
        $updateOffice->address =$request->address;
        $updateOffice->contact_person =$request->contact_person;
        $updateOffice->phone1 =$request->phone1;
        $updateOffice->phone2 =$request->phone2;
        $updateOffice->email =$request->email;
        $updateOffice->description =$request->description;
        $updateOffice->company_id =$request->company_id;
        $updateOffice->status =$request->status;


        if ($updateOffice->save()) {

            $notification = array(
                'message' => 'Update Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Save Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($deletsOffice = Officecontact::where('id','=',$request->id)->delete()) {

            $notification = array(
                'message' => 'Delete Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}
