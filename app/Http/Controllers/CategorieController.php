<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Item;
use App\SubCategorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = Categorie::where('cat_status','=',1)->get();;
        $data = Categorie::all()->sortByDesc('id');
        return view('category.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cat_name' => 'required|max:255',
            'cat_status' => 'required|numeric'
        ]);

        $data = new Categorie();

        $data->cat_name = $request->cat_name;
        $data->cat_status = $request->cat_status;

        if ($data->save()) {

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorie = Categorie::where('id','=',$id)->get();
        return view('category.edit')->with('categorie',$categorie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updatedata = Categorie::find($request->id);

        $this->validate($request,[
            'cat_name' => 'required|max:255',
            'cat_status' => 'required|numeric'
        ]);


        $updatedata->cat_name = $request->cat_name;
        $updatedata->cat_status = $request->cat_status;

        if ($updatedata->save()) {

            $notification = array(
                'message' => 'Update Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Update Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //dd($request->id);
        //$deletes = Categorie::find($request->id)->delete();

       // $deletes = Categorie::find($request->id)->delete();

        $deleteItem = Item::where('categorie','=',$request->id)->delete();
        $deletesSubCat = SubCategorie::where('cat_id',$request->id)->delete();

        if ($deletes = Categorie::find($request->id)->delete()) {

            //$delSubCat = SubCategorie::where('cat_id','=',$request->id)->delete();

            $notification = array(
                'message' => 'Successfully Delete !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Delete Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}
