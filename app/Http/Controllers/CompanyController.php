<?php

namespace App\Http\Controllers;

use App\Company;
use App\Item;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$data = Categorie::where('cat_status','=',1)->get();;
        $data = Company::all()->sortByDesc('id');
        return view('company.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[
            'company_name' => 'required|max:255',
            'company_status'=>'required'
        ]);

        $data = new Company();


        $data->company_name = $request->company_name;
        $data->company_status = $request->company_status;


        if ($data->save()) {

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategorie = Company::where('id','=',$id)->get();
        return view('company.edit')->with('subcategorie',$subcategorie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {


        $updatedata = Company::find($request->id);


        $this->validate($request,[
            'company_name' => 'required|max:255',
            'company_status'=>'required'
        ]);



        $updatedata->company_name = $request->company_name;
        $updatedata->company_status = $request->company_status;


        if ($updatedata->save()) {

            $notification = array(
                'message' => 'Update Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Update Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //dd($request->id);
        //$deletes = Categorie::find($request->id)->delete();

        $deleteItem = Item::where('company_id','=',$request->id)->delete();

        if ($deletes = Company::find($request->id)->delete()) {

            $notification = array(
                'message' => 'Successfully Delete !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Delete Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}
