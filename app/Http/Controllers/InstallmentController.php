<?php

namespace App\Http\Controllers;

use App\Installment;
use App\Item;
use App\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class InstallmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $allInstallment = Installment::all()->sortByDesc('id');

        return view('installment.allinstallment')->with('data',$allInstallment);
    }


    public function unverified()
    {
        $allInstallment = Installment::where('installment_verified','=',0)->get();

        return view('installment.allinstallment')->with('data',$allInstallment);
    }



    public function Verified($id){

        $status = Installment::find($id);

        return view('installment.changestatus',compact('status'));
    }

    public function VerifiedInstallMent(){

        $data= Installment::where('installment_verified','=',1)->get();

        return view('installment.allverivideinstallment',compact('data'));
    }



    public function VerifiedStore(Request $request){

        $find = Installment::find($request->id);

        $find->installment_verified = $request->status;

        if ($find->save()) {

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $sales_price = DB::select('select sales_price from sales where id = :id', ['id' => $id]);

       foreach ($sales_price as $sprice){

       }

       $inAmount = DB::select("SELECT amount FROM `installments` WHERE sales_id= :sales_id",['sales_id' => $id]);


        foreach ($inAmount as $sumAmount){

            @$sumAmount2 +=$sumAmount->amount;



        }

        $payableAmount = $sprice->sales_price - @$sumAmount2;


        if($sprice->sales_price > @$sumAmount2){

            $sales_id = $id;

            //return view('installment.create')->with('sales_id',$id);

            return view('installment.create',compact('sales_id','payableAmount'));


        }else {




            echo "error";
            $notification = array(
                'message' => 'Installment Complete',
                'alert-type' => 'error'
            );


            return redirect()->route('salesindex')->with($notification);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $InstalmentStore = new  Installment();

        $InstalmentStore->bank_name = $request->bank_name;
        $InstalmentStore->branch_name = $request->branch_name;
        $InstalmentStore->check_no = $request->check_no;
        $InstalmentStore->amount = $request->amount;
        $InstalmentStore->payment_method = $request->payment_method;
        $InstalmentStore->type = $request->type;
        $InstalmentStore->sales_id = $request->sales_id;
        $InstalmentStore->collection_status = $request->collection_status;
        $InstalmentStore->installment_date = date_create($request->installment_date);
        $InstalmentStore->installment_verified = $request->installment_verified;

        $findItem = Sales::where('id','=',$request->sales_id)->get();

        foreach ($findItem as $item) {
           @$itemId = $item->item_id;
        }

        //dd($itemId);

        if ($InstalmentStore->save()) {

            DB::table('sales')->where('id',$request->sales_id)->update(['status' => 1]);

            //$findItem = Sales::find('id',$request->sales_id);

           // dd($findItem);

            $changeStatus = Item::find($itemId);

            //dd($changeStatus);

            $changeStatus->salse = '1';
            $changeStatus->save();



            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Installment::where('sales_id','=',$id)->get();

        $iditem = $id;


       // return view('installment.index')->with('data',$InstallMent);

        return view('installment.index',compact('data','iditem'));
    }

    public function showData(Request $request)
    {


       // $data = Installment::where('sales_id','=',$request->installment_id)->get();


        $sales_price = DB::select('select sales_price from sales where id = :id', ['id' => $request->sales_id]);

        foreach ($sales_price as $sprice){

        }

        $inAmount = DB::select("SELECT amount FROM `installments` WHERE sales_id= :sales_id",['sales_id' => $request->sales_id]);


        foreach ($inAmount as $sumAmount){

            @$sumAmount2 +=$sumAmount->amount;



        }



        $payableAmount = @$sprice->sales_price - @$sumAmount2;


        $findInstallment = Installment::where('id',$request->installment_id)->get();

        $sales_id = $request->sales_id;


        // return view('installment.index')->with('data',$InstallMent);

        return view('installment.edit',compact('data','iditem','payableAmount','findInstallment','sales_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $sales_price = DB::select('select sales_price from sales where id = :id', ['id' => $id]);

        foreach ($sales_price as $sprice){

        }

        $inAmount = DB::select("SELECT amount FROM `installments` WHERE sales_id= :sales_id",['sales_id' => $id]);


        foreach ($inAmount as $sumAmount){

            @$sumAmount2 +=$sumAmount->amount;



        }

        $payableAmount = @$sprice->sales_price - @$sumAmount2;

        return view('installment.edit',compact('data','payableAmount'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateInstallment = Installment::find($request->id);


        $updateInstallment->bank_name = $request->bank_name;
        $updateInstallment->branch_name = $request->branch_name;
        $updateInstallment->check_no = $request->check_no;
        $updateInstallment->amount = $request->amount;
        $updateInstallment->payment_method = $request->payment_method;
        $updateInstallment->type = $request->type;
        $updateInstallment->sales_id = $request->sales_id;
        $updateInstallment->collection_status = $request->collection_status;
        $updateInstallment->installment_date = date_create($request->installment_date);
        $updateInstallment->installment_verified = $request->installment_verified;


        if ($updateInstallment->save()) {

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        $data = Installment::where('sales_id','=',$request->sales_id)->get();

        $iditem = $request->sales_id;


        return view('installment.index',compact('data','notification','iditem'));

        //return back()->with($notification);





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {




        if ($deleteInstallment = Installment::find($request->id)->delete()) {

            $notification = array(
                'message' => 'Delete Installment!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);


    }
}
