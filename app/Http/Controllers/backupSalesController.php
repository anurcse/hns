<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Company;
use App\Customer;
use App\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;


class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sales.create');
    }


    public function myform()

    {

//        $state = DB::table("categories")->select("cat_name","id");
        //$state = DB::table('categories')->get();
        $company = Company::all();
        $category = Categorie::all();





        return view('sales.create',compact('company','category'));
        //return view('sales.create')->with('company',$company);



    }


    public function myformAjax($id)

    {


        $cities = DB::table("items")

            ->where("categorie",$id)->get(['item_name', 'id']);




        return json_encode($cities);






    }

    public function myformAjax2($id,$id2)

    {


        $cities = DB::table("items")

            ->where("categorie",$id)->where("company_id",$id2)->get(['item_name', 'id']);




        return json_encode($cities);






    }






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request,[
            'rv_no'      => 'nullable|numeric',
            'amount'     => 'required|numeric',
            'ref_no'     => 'nullable|alpha_dash',
            'date'       => 'required|date',
            'receive_ac' => 'required|numeric',
            'credit_ac'  => 'required|numeric',
            'payee_name' => 'nullable',
            'desc'       => 'nullable|max:300'
        ]);


        $company = Company::all();
        $category = Categorie::all();
        $customer = Customer::where('email','=',$request->email)->get();



        if(count($customer) > 0 ) {

            $notification = array(
                'message' => 'Email alredy !',
                'alert-type' => 'success'
            );

            $old = $request->all();

            //dd('not ok');

            //dd($old);

            return view('sales.create',compact('old','company','category','notification'));

           // return back()->with('old','company','category','notification');

            //return redirect('salescreate')->with('old','company','category','notification');

        }else{



            //dd('ok');


        $customer = Customer::pluck('id')->last();
        $customer_last = count($customer) > 0 ? $customer + 1 : 1;

        $Sales = new  Sales();
        $Sales->item_id = $request->item_id;
        $Sales->customer_id = $customer_last;
        //dd($Sales->customer_id);
        $Sales->user_id = $request->user_id;
        $Sales->commission_status = '0';
        $Sales->sales_price = $request->sales_price;
        $Sales->sales_code = uniqid();
        $Sales->status = '0';


        ///////////////// Customer ////////////////////









        $Customer = new Customer();

        $Customer->name = $request->customer_name;
        $Customer->address = $request->address;
        $Customer->phone = $request->phone;
        $Customer->email = $request->email;
        $Customer->n_id = $request->nid;
        $Customer->tin_id = $request->tin;
        $Customer->reg_no = $request->reg_no;

        if($Customer->save()){
            $Sales->save();
        }

        $notification = array(
            'message' => 'Successfully Save !',
            'alert-type' => 'success'
        );



        }

        return back()->with($notification);




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
