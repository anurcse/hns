<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Installment;
use App\Item;
use App\Photo;
use App\Sales;
use App\SubCategorie;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');



    }

    public function index()
    {
        //$data = Categorie::where('cat_status','=',1)->get();;
        $data = Item::all()->sortByDesc('id');
        return view('item.index')->with('data',$data);
    }


    public function itemstatus($id)
    {

        $data = Item::where('id','=',$id)->get();

        //dd($data);

        $findsales = Sales::where('item_id','=',$id)->get();

       // dd($findsales);

       // return view('item.status')->with('data',$data);

        return view('item.status',compact('data','findsales'));

    }

    public function updateitemstatus(Request $request)
    {

        //dd($request->all());

        $sales =  Sales::where('id','=',$request->sales_id)->get();

        $installment = Installment::where('sales_id','=',$request->sales_id)->get();


        foreach ($sales as $s){

        }

        foreach ($installment as $in){

            @$sumin  += $in->amount;
        }

//        echo $sumin;
//
//        echo $s->sales_price;

        $find70 = ($s->sales_price * 70)/100;

        //dd($find70);



        $findstatus = Item::find($request->id);



        $findstatus->salse = $request->status;




        if (@$sumin >= $find70) {

            $findstatus->save();

            $notification = array(
                'message' => 'Successfully Update Status !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Installment less then 70%!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('item.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $item = new  Item();
        $item->categorie = $request->categorie;
        $item->subcategorie =$request->subcategorie;
        $item->company_id = $request->company_id;
        $item->item_name = $request->item_name;
        $item->year = $request->year;
        $item->description =$request->description;
        $item->specification = $request->specification;
        $item->model_no = $request->model;
        $item->price = $request->price;
        $item->item_status = $request->item_status;
        $item->chassis_number = $request->chassis_number;
        $item->user_id  = $request->user_id;
        $item->salse  = '0';




//        if($item->save()){
//
//        }else{
//            DB::table('photos')->whereIn('item_id', $itesm_last)->delete();
//        }


        if ($item->save()) {


            $items = Item::pluck('id')->last();

            $itesm_last = count($items) > 0 ? $items : 1;


            $picture = '';
            if ($request->hasFile('image')) {
                $files = $request->file('image');
                foreach($files as $file){
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $picture = date('His').$filename;
                    //$destinationPath = base_path() . '\public\image';
                    $destinationPath = 'image';
                    $file->move($destinationPath, $picture);

                    $photo = new Photo();
                    $photo->photo_url = $picture;
                    $photo->item_id =$itesm_last;
                    $photo->save();
                }
            }

            $notification = array(
                'message' => 'Successfully Save !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $findItem = Item::where('id','=',$id)->get();
        $subcat   = SubCategorie::where('subcat_status','=',1)->get();

        $findImage = Photo::where('item_id','=',$id)->get();

        return view('item.view',compact('findItem','findImage','subcat'));

    }


    /////////////////////////////////////////////////////////////


    public function myform()

    {

//        $state = DB::table("categories")->select("cat_name","id");
            //$state = DB::table('categories')->get();
        $state = Categorie::all();



        //return view('item.create',compact('states'));
        return view('item.create')->with('states',$state);



    }


    /**

     * Get Ajax Request and restun Data

     *

     * @return \Illuminate\Http\Response

     */

    public function myformAjax($id)

    {

        $cities = DB::table("sub_categories")

            ->where("cat_id",$id)->where('subcat_status','=',1)->get(['subcat_name', 'id']);

           // ->lists("subcat_name","id");

        //$cities = SubCategorie::where('cat_id', $id)->get(['subcat_name', 'id']);
       // $cities = SubCategorie::where('cat_id', $id)->get();


        return json_encode($cities);

       // return view('item.create')->with('cities',$cities);

    }


    ///////////////////////////////////////////////////////////////

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editItem = Item::where('id','=',$id)->get();
        $editPhoto = Photo::where('item_id','=',$id)->get();
        $subcatall = SubCategorie::where('subcat_status','=',1)->get();




        return view('item.edit',compact('editItem','editPhoto','subcatall'));
        //return view('item.edit')->with('editItem',$editItem);
    }

    public function delephoto($id){

        $findImgeDelet = Photo::find($id);
        unlink("image/".$findImgeDelet->photo_url);
        $findImgeDelet->delete();

        $notification = array(
            'message' => 'Successfully Delete Photo',
            'alert-type' => 'success'
        );


//        return redirect()->route('itemedit', ['id' => $findImgeDelet->item_id]);

        return redirect()->route('itemedit', ['id' => $findImgeDelet->item_id])->with($notification);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */






    public function update(Request $request)
    {

        if($request->hasFile('image')){

            $item =Item::find($request->id);
            $item->categorie = $request->categorie;
            $item->subcategorie = $request->subcategorie;
            $item->company_id = $request->company_id;
            $item->item_name = $request->item_name;
            $item->year = $request->year;
            $item->description =$request->description;
            $item->specification = $request->specification;
            $item->model_no = $request->model;
            $item->price = $request->price;
            $item->item_status = $request->item_status;
            $item->chassis_number = $request->chassis_number;
            $item->user_id  = '1';




            if ($item->save()) {


//                $deletImagfind = DB::table('photos')->where('item_id', $request->id)->get();
//
//
//                foreach ($deletImagfind as $all){
//
//                    unlink(base_path() ."/public/image/".$all->photo_url);
//                }

//                $deletImag = DB::table('photos')->where('item_id', $request->id)->delete();



                $picture = '';
                if ($request->hasFile('image')) {
                    $files = $request->file('image');
                    foreach($files as $file){
                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $picture = date('His').$filename;
                        //$destinationPath = base_path() . '\public\image';
                        $destinationPath = 'image';
                        $file->move($destinationPath, $picture);

                        $photo = new Photo();
                        $photo->photo_url = $picture;
                        $photo->item_id =$request->id;
                        $photo->save();
                    }
                }

                $notification = array(
                    'message' => 'Update Successfully',
                    'alert-type' => 'success'
                );

            }else {
                echo "error";
                $notification = array(
                    'message' => 'Error!',
                    'alert-type' => 'error'
                );
            }

            return back()->with($notification);

        }else{

            $item =Item::find($request->id);
            $item->categorie = $request->categorie;
            $item->subcategorie =$request->subcategorie;
            $item->company_id = $request->company_id;
            $item->item_name = $request->item_name;
            $item->year = $request->year;
            $item->description =$request->description;
            $item->specification = $request->specification;
            $item->model_no = $request->model;
            $item->price = $request->price;
            $item->item_status = $request->item_status;
            $item->chassis_number = $request->chassis_number;
            $item->user_id  = '1';


            if ($item->save()) {

                $notification = array(
                    'message' => 'Update Successfully',
                    'alert-type' => 'success'
                );

            }else {
                echo "error";
                $notification = array(
                    'message' => 'Error!',
                    'alert-type' => 'error'
                );
            }

            return back()->with($notification);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        //dd($request->id);
        //$deletes = Categorie::find($request->id)->delete();


        if ($deletes = Item::find($request->id)->delete()) {

            $deletImagfind = DB::table('photos')->where('item_id', $request->id)->get();


            foreach ($deletImagfind as $all){

                unlink(base_path() ."/public/image/".$all->photo_url);
            }

            $deletImag = DB::table('photos')->where('item_id', $request->id)->delete();
            $notification = array(
                'message' => 'Successfully Delete !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Delete Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}
