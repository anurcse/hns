<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserControler extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = User::all()->sortByDesc('id');
        return view('user.index')->with('data',$allUser);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $addUser = new User();
        $addUser->first_name = $request->first_name;
        $addUser->last_name = $request->last_name;
        $addUser->birthday = date_create($request->birthday);
        $addUser->gender = $request->gender;
        $addUser->name = $request->name;
        $addUser->email = $request->email;
        $addUser->password = bcrypt($request->password);
        $addUser->role = $request->role;

        $addUser->status = $request->status;

        if ($addUser->save()) {

            $notification = array(
                'message' => 'Save Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Save Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $userData =  User::where('id','=',$id)->get();



        return view('user.view')->with('data',$userData);

       // return view('user.create');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $data = User::where('id','=',$id)->get();



        return view('user.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $updateUser = User::find($request->id);
        $updateUser->first_name = $request->first_name;
        $updateUser->last_name = $request->last_name;
        $updateUser->birthday = date_create($request->birthday);
        $updateUser->gender = $request->gender;
//        $updateUser->name = $request->name;
        $updateUser->email = $request->email;
        $updateUser->status = $request->status;

//dd($updateUser->status = $request->role);

        if ($updateUser->save()) {

            $notification = array(
                'message' => 'Update Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Save Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        
        if ($deletsUser = User::where('id','=',$request->id)->where('role','!=',1)->delete()) {

            $notification = array(
                'message' => 'Delete Successfully!',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'This is Admin User!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);


    }


    public function userChangePassword($id){

        $data = User::find($id);

        return view('user.changepassword',compact('data'));

    }

    public function userPasswordUpdate(Request $request){


        $update = User::find($request->id);

        $update->password =  bcrypt($request->password);

        if($update->save())
        {
            $notification = array(
                'message' => 'New Password Set Successfully!',
                'alert-type' => 'success'
            );

            return back()->with($notification);

        }else{

            echo "error";
            $notification = array(
                'message' => 'This is Admin User!',
                'alert-type' => 'error'
            );

        }



    }
}
