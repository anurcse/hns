<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Company;
use App\Customer;
use App\Installment;
use App\Item;
use App\Photo;
use App\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;



class SalesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        @$saleStatus = Sales::where('status','=',1)->get();


//        $ThatTime ="14:08:10";
//        $todaydate = date('Y-m-d');
//        $time_now=mktime(date('G'),date('i'),date('s'));
//        $NowisTime=date('G:i:s',$time_now);
//        if($NowisTime >= $ThatTime) {
//            echo "ok";
//        }

       @$today = date('Y-m-d');

       //$UpdateDate = strtotime("+72 hours", strtotime($today));






//        echo $idate="2013-09-25 09:29:44";
//
//        $effectiveDate = strtotime("+40 minutes", strtotime($idate));
//
//        echo date("Y-m-d h:i:s",$effectiveDate);




//$date1=date_create("2013-03-15");
//$date2=date_create("2013-12-12");
//$diff=date_diff($date1,$date2);
//echo $diff->format("%R%a days");





foreach ($saleStatus as $sale){

           if($sale->status == 1){

               $datetime1 = date_create($sale->salesdate);
               $datetime2 = date_create($today);

               $interval = date_diff($datetime1, $datetime2);

//               echo $interval->format('%a');
//die();
               if( $interval->format('%a') > 3){



//                $itemStatus = Item::where('salse','=',1)->get();
//
//                dd($itemStatus);

//                DB::table('items')->update(['salse' => 0]);

//                DB::table('items')->where('id',$sale->item_id)->update(['salse' => 0]);

                  $dd = Item::find($sale->item_id);

                  $dd->salse = 0;

                  if($dd->save()){

                      $findSales = Sales::where('item_id',$sale->item_id)->delete();

                      $findCustomer = Customer::where('id','=',$sale->customer_id)->delete();


                  }



                      //

                  //var_dump($dd) or die();



//                   $itemStatus->salse = 0;
//
//                   $itemStatus->save();


               }

           }
       }




    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = Sales::all();

       $installment = Installment::all();


        foreach($installment as $instalId) {


            $installment2[]  = $instalId->sales_id;

        }

        //return view('sales.index')->with('data',$data);

        return view('sales.index',compact('data','installment2'));

        


    }


    public function index2()
    {
        $user_id = \Auth::user()->id;

        $data = Sales::where('user_id','=',$user_id)->get();



        $installment = Installment::all();


        foreach($installment as $instalId) {


            $installment2[]  = $instalId->sales_id;

        }

        //return view('sales.index')->with('data',$data);

        return view('sales.index2',compact('data','installment2'));




    }

    public function salesAvailable(){

        $data = Item::where('salse','=',0)->get();

        $photo = Photo::all();

        return view('sales.avaliableitem',compact('data','photo'));

        //return view('sales.avaliableitem')->with('data',$Available);
    }

    public function salessingle($id){



        $data = Item::find($id);



        $photo = Photo::where('item_id','=',$id)->get();



        return view('sales.salesAview',compact('data','photo'));

    }


    public function salesToAvaliable($id){

        $savali = Item::find($id);

        $company = Company::where('company_status' ,'=',1)->get();
        $category = Categorie::where('cat_status','=',1)->get();

        return view('sales.create2',compact('savali','company','category'));



    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('sales.create');
    }


    public function myform()

    {

//      $state = DB::table("categories")->select("cat_name","id");
        //$state = DB::table('categories')->get();
        //$company = Company::all();
        //$category = Categorie::all();

        $company = Company::where('company_status' ,'=',1)->get();
        $category = Categorie::where('cat_status','=',1)->get();


        return view('sales.create',compact('company','category'));
        //return view('sales.create')->with('company',$company);



    }


    public function myformAjax($id)

    {


//        $cities = DB::table("items")
//
//            ->where("categorie",$id)->where('salse','=',0 or 1)->get(['item_name', 'id']);

        $cities = DB::table("items")

            ->where("company_id",$id)->where('salse','=',0)->get(['item_name', 'id']);




        return json_encode($cities);






    }

    public function myformAjax2($id,$id2)

    {


//        $cities = DB::table("items")
//
//            ->where("categorie",$id2)->where("company_id",$id)->where('salse','=',0 or 1)->get(['item_name', 'id']);

//        $cities = DB::table("items")
//
//            ->where("company_id",'=',$id and "categorie",'=',$id2)->where('salse','=',0)->get(['item_name', 'id']);
//

        $cities = Item::where('company_id','=',$id)
            ->where('categorie','=',$id2)
            ->where('salse','=',0)
            ->get(['item_name', 'id']);









        return json_encode($cities);






    }






    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        //$company = Company::all();
       // $category = Categorie::all();

        $company = Company::where('company_status','=',1)->get();
        $category = Categorie::where('cat_status','=',1)->get();
        $email = Customer::where('email','=',$request->email)->get();

        //dd($request->email);

        $n_id = Customer::where('n_id','=',$request->nid)->get();
        $tin = Customer::where('tin_id','=',$request->tin)->get();
        $reg_no = Customer::where('reg_no','=',$request->reg_no)->get();


        if(count($email) > 0){
            $email1 = $request->email;

            //dd($request->email);
        }
        if(count($n_id) > 0){
            $nid1 = $request->nid;
            //dd($nid1);
        }
        if(count($tin) > 0){
            $tin1 = $request->tin;
            //dd($tin1);
        }
        if(count($reg_no) > 0){
            $reg1 = $request->reg_no;

            //dd($reg1);
        }

        if(count($email) > 0 || count($n_id) > 0 || count($tin) > 0 || count($reg_no) > 0 ) {

            $old = $request->all();

//            $email1 = $request->eamil;
//            $nid1 = $request->nid;
//            $tin1 = $request->tin;
//            $reg1 = $request->reg_no;

            return view('sales.create',compact('old','company','category','email1','nid1','tin1','reg1'));

            //return back()->with($notification,$old,$company,$category);

            //return redirect('salescreate')->with($old,$company,$category,$notification);

        }else{



            //dd('ok');


//        $customer = Customer::pluck('id')->last();
//
//
//        $customer_last = count($customer) > 0 ? $customer + 1 : 1;

        //dd($customer_last);




        $Sales = new  Sales();
        $Sales->item_id = $request->item_id;
        //$Sales->customer_id = $request->customer_last;

        //dd($Sales->customer_id);
        $Sales->company_id = $request->company_id;
        $Sales->cat_id = $request->category_id;
        $Sales->salesdate = date('Y:m:d');
            //dd($Sales->salesdate);
        $Sales->user_id = $request->user_id;
        $Sales->commission_status = '0';
        $Sales->sales_price = $request->sales_price;
        $Sales->sales_code = uniqid();
//      $Sales->status = $request->status;
        $Sales->status = '0';


//      dd($request->status);


//        $itemstatus = Item::find($request->item_id);
//
//        $itemstatus->salse = $request->status;
//
//
//        $itemstatus->save();


        ///////////////// Customer ////////////////////


        $Customer = new Customer();

        $Customer->name = $request->customer_name;
        $Customer->address = $request->address;
        $Customer->phone = $request->phone;
        $Customer->email = $request->email;
        $Customer->n_id = $request->nid;
        $Customer->tin_id = $request->tin;
        $Customer->reg_no = $request->reg_no;

        if($Customer->save()){

            $Sales->customer_id = $Customer->id;

           // dd($Sales->customer_id);


            if($Sales->save()){

            $itemstatus = Item::find($request->item_id);
           // $itemstatus->salse = $request->status;
            $itemstatus->salse = 2;
            $itemstatus->save();

            }else{

                $lastdel = $request->customer_last - 1 ;

                $delCustomer = Customer::find($lastdel)->delete();

            }

        }

        $notification = array(
            'message' => 'Successfully Save !',
            'alert-type' => 'success'
        );

        }

        return back()->with($notification);




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $findSales = Sales::where('id','=',$id)->get();

        foreach ($findSales as $item){

        }

        $findItem = Item::where('id','=',$item->item_id)->get();

        $findCustomer = Customer::where('id','=',$item->customer_id)->get();





        return view('sales.salesview',compact('findSales','findItem','findCustomer'));
    }


    public function SalesStatus($id){
        $getItem = Sales::find($id)->get();
        return view('sales.status')->with('data',$getItem);
    }

    public function SalesStatusUpdate(Request $request){

            $find = Sales::find($request->id);
            $find->status = $request->status;
            $find->save();

        $notification = array(
            'message' => 'Sales Status Update!',
            'alert-type' => 'success'
        );



return back()->with($notification);


}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $company = Company::all();
//        $category = Categorie::all();

        $company = Company::where('company_status' ,'=',1)->get();
        $category = Categorie::where('cat_status','=',1)->get();

        $salesItem = Sales::where('id' ,'=',$id)->get();

        //$items3 = Item::where('salse','=',1 or 0)->get();

        //$items3 = Item::where('salse','=', 2)->get();

        $items3 = Item::where('salse','!=', 0)->get();


       // dd($items3);

        foreach ($salesItem as $i){
           $i->company_id;
        }

        $itemId = $i->item_id;






        //$findCustomer = Customer::where('id','=',$cus)->get();

        return view('sales.edit',compact('company','category','salesItem','i','itemId','items3'));
    }

    public function salesUpdate(Request $request){


        $salesUpdate = Sales::find($request->id);

        $salesUpdate->item_id = $request->item_id;
        $salesUpdate->company_id = $request->company_id;
        $salesUpdate->cat_id = $request->category_id;
        $salesUpdate->user_id = $request->user_id;
        $salesUpdate->commission_status = '0';
        $salesUpdate->sales_price = $request->sales_price;

        //dd($request->oldItem);



        if ($salesUpdate->save()) {

            $itemSales = Item::find($request->item_id);
            $itemSales->salse = '2';
            $itemSales->save();

            if($request->item_id != $request->oldItem){


                $oldItemchage = Item::find($request->oldItem);

                $oldItemchage->salse = '0';
                $oldItemchage->save();

                $notification = array(
                    'message' => 'Update Successfully!',
                    'alert-type' => 'success'
                );

            }else{

                $notification = array(
                    'message' => 'Update Successfully!',
                    'alert-type' => 'success'
                );

            }


        }else {
            echo "error";
            $notification = array(
                'message' => 'Update Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $salesItem = Sales::where('id' ,'=',$request->id)->get();

        foreach ($salesItem as $i){
            $i->company_id;
        }

        $company = Company::all();
        $category = Categorie::all();

        //$email = Customer::where('email','=',$request->email)->where('id','=',$request->id)->get();



        //$n_id = Customer::where('n_id','=',$request->nid)->where('id','=',$request->id)->get();
        //$tin = Customer::where('tin_id','=',$request->tin)->where('id','=',$request->id)->get();
        //$reg_no = Customer::where('reg_no','=',$request->reg_no)->where('id','=',$request->id)->get();

        $email = DB::table('customers')->where('email', '=',$request->email)->first();
        $n_id = DB::table('customers')->where('n_id', '=',$request->nid)->first();
        $tin = DB::table('customers')->where('tin_id', '=',$request->tin)->first();
        $reg_no = DB::table('customers')->where('reg_no', '=',$request->reg_no)->first();


//echo $email->id;
//        echo '<br>';
//echo $email->email;
//echo '<br>';
//echo $n_id->id;
//echo '<br>';
//echo $n_id->n_id;
//        echo '<br>';
//
//echo $tin->id;
//echo '<br>';
//echo $tin->tin_id;
//        echo '<br>';
//
//echo $reg_no->id;
//
//        echo '<br>';
//        echo $reg_no->reg_no;
//
//die();


        if(@$email->id != $request->id){
            @$email1 = $email->id;

           // dd($email1);
        }else{
            @$email1 = $request->id;
        }
        if($n_id->id != $request->id){
            @$nid1 = $request->id;
            //dd($nid1);
        }else{
            @$nid1 = $n_id->id;
        }
        if($tin->id != $request->id){
            @$tin1 = $request->id;
            //dd($tin1);
        }else{
            @$tin1 = $tin->id;
        }
        if($reg_no->id != $request->id){
            @$reg1 = $request->id;

            //dd($reg1);
        }else{
            @$reg1 = $reg_no->id;
        }

        if($email->id != $request->id || $n_id->id != $request->id || $tin->id != $request->id || $reg_no->id != $request->id ) {


              dd("not ok");
            return view('sales.edit',compact('old','company','category','email1','nid1','tin1','reg1','salesItem'));


        }else{


        $findSeals = Sales::find($request->id);

        $findSeals->item_id = $request->item_id;
        $findSeals->customer_id = $request->customerId;
        $findSeals->company_id = $request->company_id;
        $findSeals->cat_id = $request->category_id;
        $findSeals->user_id = $request->user_id;
        //$findSeals->commission_status = '0';
        $findSeals->sales_price = $request->sales_price;
       // $findSeals->sales_code = uniqid();
       // $findSeals->status = '0';



        $findCustomer = Customer::find($request->customerId);

        $findCustomer->name = $request->customer_name;
        $findCustomer->address = $request->address;
        $findCustomer->phone = $request->phone;
        $findCustomer->email = $request->email;
        $findCustomer->n_id = $request->nid;
        $findCustomer->tin_id = $request->tin;
        $findCustomer->reg_no = $request->reg_no;







        if($findCustomer->save()){
            $findSeals->save();
        }

        $notification = array(
            'message' => 'Successfully Update !',
            'alert-type' => 'success'
        );



    }

return back()->with($notification);







    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {


        if ($deleteItem = Sales::where('id','=',$request->id)->delete()) {

            $notification = array(
                'message' => 'Successfully Delete !',
                'alert-type' => 'success'
            );

        }else {
            echo "error";
            $notification = array(
                'message' => 'Delete Error!',
                'alert-type' => 'error'
            );
        }

        return back()->with($notification);
    }
}
