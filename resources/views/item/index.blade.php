@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

{{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>All Items</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



    @foreach($data as $single)



    @endforeach


            <div class="card-box">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('itemcreate')}}" class="btn btn-default">Add Item</a>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            {{--<div class="dropdown pull-right">--}}
                                {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                                    {{--<i class="zmdi zmdi-more-vert"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Action</a></li>--}}
                                    {{--<li><a href="#">Another action</a></li>--}}
                                    {{--<li><a href="#">Something else here</a></li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">Separated link</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}

                            {{--<h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>--}}

                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Picture</th>
                                    <th>Chassis Number</th>
                                    <th>Model</th>
                                    <th>Category</th>
                                    <th>Year</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                                <tbody>

                                @php
                                    $i=1;
                                @endphp
                                @foreach($data as $single)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$single->item_name}}</td>

                                        <td>

                                            @foreach($photo as $pho)

                                                @if($single->id == $pho->item_id )

                                                    <img src="{{asset("/image/$pho->photo_url")}}" class="thumb-md">

                                                    @break




                                                @endif

                                            @endforeach
                                        </td>




                                        <td>{{$single->chassis_number}}</td>
                                        <td>{{$single->model_no}}</td>
                                        
                                        
                                        <td>
                                        
                                        @foreach($categorie as $cat)
                                        
                                        @if($single->categorie == $cat->id)
                                            {{$cat->cat_name}}
                                            
                                        @endif
                                        
                                        @endforeach
                                        </td>
                                        <td>{{$single->year}}</td>




                                        <td>

                                            @if ($single->salse == '0')
                                                <span style="color: green">Available</span>
                                            @elseif($single->salse == '2')
                                                <span style="color: yellow">Booking</span>
                                            @elseif($single->salse == '1')
                                                <span style="color: red;">Sold</span>
                                            @else
                                                <span>Delivery</span>
                                            @endif


                                        </td>

                                        <form action="{{route('itemdelete')}}"  method="post">
                                            {{ csrf_field() }}

                                            <input type="hidden"  name="id" value="{{$single->id}}">

                                        <td>
                                            <a href="itemview/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-warning m-b-5">View</a>
                                            @if($single->salse == '1' || $single->salse == '3' || $single->salse == '2')
                                            {{--@if($single->salse == '1')--}}

                                            @else
                                                <a href="itemedit/{{$single->id}}" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</a>
                                                @if(Auth::user()->role == 1)
                                                    <button type="submit" class="btn btn-danger btn-trans waves-effect w-md waves-danger m-b-5 delete">Delete</button>
                                                @endif
                                            @endif

                                        </td>
                                        </form>
                                    </tr>


                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>


            </div>





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?" +
                "delete Company prodouct");
        });
    </script>

{{--@include('layouts.datatablejs');--}}


@endsection