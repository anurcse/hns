@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />



    <!-- Editatable  Css-->

    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-datatables-editable/datatables.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/toastr/toastr.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/custombox/dist/custombox.min.css') }}">--}}
    <style>
        .required{
            color:red;
        }
        dl{
            padding:15px;
        }
        dd{
            text-align: left;
        }
        .dataTables_length{
            display: none;
        }
    </style>
@endsection

@section('page-header')
    <h2>Edit Item</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">

                <div class="col-sm-12">

                    <div class="card-box">


                        <h4 class="header-title m-t-0 m-b-30">Edit Item</h4>

                        <form action="{{route('itemupdate')}}" data-parsley-validate novalidate method="post" enctype="multipart/form-data">

                            {{ csrf_field() }}

                            @foreach($editItem as $list)
                            @endforeach

                            <input type="hidden" name="id" value="{{$list->id}}">

                            <div class="form-group">
                                <label for="userName">Item Name*</label>
                                <input type="text" name="item_name" parsley-trigger="change" required
                                       placeholder="Enter Item Name" value="{{$list->item_name}}"  class="form-control" >
                            </div>

                            <div class="form-group">
                                <label for="userName">Category</label>
                                {{--<input type="text" name="categorie" parsley-trigger="change" required--}}
                                {{--placeholder="Enter Item Name"  class="form-control" >--}}
                                <select name="categorie" id="" class="form-control">
                                    @foreach($categorie as $cat)
                                        <option value="{{$cat->id}}" @if($list->categorie == $cat->id) selected @endif>{{$cat->cat_name}}</option>
                                    @endforeach
                                </select>

                            </div>

                            @php

                               // foreach ($subcatall as $scat) {

                                // echo $scat->subcat_name . '<br>';
                                 //}
                           // die();
                            @endphp
                            <div class="form-group">
                                <label for="userName">Sub Category*</label>
                                <select name="subcategorie" class="form-control">



                                    {{--@foreach($subcategoriess as $sub)--}}
                                        {{--<option value="{{$sub->id}}" @if($sub->id == $list->subcategorie)  @else style="display: none"  @endif>{{$sub->subcat_name }}</option>--}}
                                    {{--@endforeach--}}

                                        @foreach($subcatall as $sub)
                                           <option value="{{$sub->id}}" @if($list->subcategorie == $sub->id ) selected @endif>{{$sub->subcat_name}}</option>
                                        @endforeach

                                </select>





                            </div>
                            <div class="form-group">
                                <label for="userName">Company Name</label>
                                {{--<input type="text" name="company_id" parsley-trigger="change" required--}}
                                {{--placeholder="Enter Item Name"  class="form-control" >--}}
                                <select name="company_id" class="form-control">
                                    @foreach($company as $com)
                                        <option value="{{$com->id}}" @if($list->company_id == $com->id) selected @endif >{{$com->company_name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="userName">Year</label>
                                <input type="text" name="year" parsley-trigger="change" required
                                       placeholder="Enter Item Name"  value="{{$list->year}}" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Description</label>
                                {{--<input type="text" name="description" parsley-trigger="change" required--}}
                                {{--placeholder="Enter Item Name"  class="form-control" >--}}
                                <textarea name="description" class="form-control">{{$list->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="userName">Specification</label>
                                {{--<input type="text" name="specification" parsley-trigger="change" required--}}
                                {{--placeholder="Enter Item Name"  class="form-control" >--}}
                                <textarea name="specification" class="form-control">{{$list->specification}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="userName">Model</label>
                                <input type="text" name="model" parsley-trigger="change" required
                                       placeholder="Enter Item Name" value="{{$list->model_no}}"  class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Price</label>
                                <input type="text" name="price" parsley-trigger="change" required
                                       value="{{$list->price}}"   class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Chassis Number</label>
                                <input type="text" name="chassis_number" parsley-trigger="change" required
                                       value="{{$list->chassis_number}}"  class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Images</label>
                                {{--<input type="file" name="item_name" parsley-trigger="change" required--}}
                                {{--class="form-control" >--}}
                                <input type="file"  name="image[]" placeholder="Upload Image" multiple="true">
                            </div>

                            <div class="form-group">

                                @if($editPhoto != NULL)
                                    @foreach($editPhoto as $img)


                                    {{--<a href="#" onclick="myFunction()" >Delete</a>--}}




                                    <img src="{{asset("/image/$img->photo_url")}}" class="thumb-md">
                                    <a href="/sts/itemphotodel/{{$img->id}}" class="delete" >X</a>






                                    @endforeach
                                @endif

                            </div>


                            <div class="form-group">
                                <label for="emailAddress">Status</label>
                                <select name="item_status" id="" class="form-control">
                                   @if($list->item_status == '1')
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                   @else
                                        <option value="0">Inactive</option>
                                        <option value="1">Active</option>
                                   @endif
                                </select>
                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Submit
                                </button>
                                {{--<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">--}}
                                {{--Cancel--}}
                                {{--</button>--}}
                            </div>

                        </form>
                    </div>




                </div><!-- end col -->





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->












@endsection

@section('script')


    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?");
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function() {

            $('select[name="categorie"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '/sts/myform/ajax/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {




                            $('select[name="subcategorie"]').empty();

                            $.each(data, function(key, value) {

                                //alert(data);

                                $('select[name="subcategorie"]').append('<option value="'+ value.id +'">'+ value.subcat_name +'</option>');

                            });


                        }

                    });

                }else{

                    $('select[name="subcategorie"]').empty();

                }

            });

        });





    </script>





@endsection