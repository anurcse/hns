@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

{{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View Item</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



    @foreach($findItem as $single)


    @endforeach




        {{--categorie--}}
        {{--subcategoriess--}}
        {{--company--}}
        {{--users--}}

            <div class="card-box">

                <div class="row">
                    <div class="col-sm-12">

                        <table id="datatable-buttons" class="table table-striped table-bordered">

                            <tr>
                                <td>Type</td>
                                <td>

                                @foreach($categorie as $cat)
                                    @if($cat->id == $single->categorie)
                                        {{$cat->cat_name}}
                                    @endif
                                @endforeach


                            </tr>
                            <tr>
                                <td width="30%">Item Name</td>
                                <td>{{$single->item_name}}</td>
                            </tr>

                            {{--<tr>--}}
                                {{--<td>Sub Category</td>--}}
                                {{--<td>--}}



                                    {{--@foreach($subcat as $subcats)--}}
                                        {{--@if($subcats->id == $single->subcategorie)--}}
                                            {{--{{$subcats->subcat_name}}--}}
                                        {{--@endif--}}
                                    {{--@endforeach--}}

                                {{--@foreach($subcat as $sub)--}}

                                    {{--@if($single->subcategorie == $sub->id)--}}
                                        {{--{{$sub->subcat_name}}--}}
                                        {{--@endif--}}

                                {{--@endforeach--}}



                                {{--@php--}}

                                    {{--//echo $subcats->id;--}}

                                  {{--//  echo $single->subcategorie;--}}
                                  {{--//   die();--}}



                                {{--@endphp--}}



                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Company Name</td>--}}
                                {{--<td>--}}


                                        {{--@foreach($company as $com)--}}
                                            {{--@if($com->id == $single->company_id)--}}
                                                {{--{{$com->company_name}}--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}


                            {{--</tr>--}}
                            <tr>
                                <td>Year</td>
                                <td>{{$single->year}}</td>
                            </tr>
                            <tr>
                                <td>Description</td>
                                <td>{{$single->description}}</td>
                            </tr>
                            <tr>
                                <td>Specification</td>
                                <td>{{$single->specification}}</td>

                            </tr>
                            <tr>
                                <td>Model</td>
                                <td>{{$single->model_no}}</td>
                            </tr>
                            {{--<tr>--}}
                                {{--<td>Price</td>--}}
                                {{--<td>{{$single->price}}</td>--}}
                            {{--</tr>--}}
                            <tr>
                                <td>Chassis Number</td>
                                <td>{{$single->chassis_number}}</td>
                            </tr>
                            <tr>
                                <td>Main Picture</td>
                                <td>
                                    @php
                                      // $a = base_path()."/public/image/";
                                    @endphp
                                    @foreach($findImage as $img)

                                    <img src="{{asset("/image/$img->photo_url")}}" width="250" >



                                    @endforeach

                                </td>
                            </tr>




                        </table>

                        <a href="{{route('itemindex')}}" >Back</a>

                    </div><!-- end col -->
                </div>

            </div>


            </div>





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

{{--@include('layouts.datatablejs');--}}


@endsection