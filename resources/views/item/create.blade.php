@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />



    <!-- Editatable  Css-->

    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-datatables-editable/datatables.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/toastr/toastr.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/custombox/dist/custombox.min.css') }}">--}}
    <style>
        .required{
            color:red;
        }
        dl{
            padding:15px;
        }
        dd{
            text-align: left;
        }
        .dataTables_length{
            display: none;
        }
    </style>
@endsection

@section('page-header')
    <h2>Create Item</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">

                <div class="col-sm-12">

                        <div class="card-box">


                            <h4 class="header-title m-t-0 m-b-30">Create Item</h4>

                            <form action="{{route('itemcreate')}}" data-parsley-validate=""  method="post" enctype="multipart/form-data">

                                {{ csrf_field() }}




                                <div class="form-group">
                                    <label for="userName">Item Name*</label>
                                    <input type="text" name="item_name" parsley-trigger="change" required
                                           placeholder="Enter Item Name"  class="form-control" minlength="3" >
                                </div>

                                <div class="form-group">
                                    <label for="userName">Category</label>

                                    <select name="categorie" id="" class="form-control">
                                        @foreach($categorie as $cat)
                                            <option value="{{$cat->id}}">{{$cat->cat_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="userName">Sub Category*</label>


                                    <select name="subcategorie" class="form-control">

                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="userName">Company Name</label>

                                    <select name="company_id" class="form-control">
                                        @foreach($company as $com)
                                            <option value="{{$com->id}}">{{$com->company_name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="form-group">
                                    <label for="userName">Year</label>
                                    <input type="text" name="year" data-parsley-type="number" parsley-trigger="change" required
                                           placeholder="Enter Year"  class="form-control" >

                                </div>
                                <div class="form-group">
                                    <label for="userName">Description</label>

                                    <textarea name="description" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="userName">Specification</label>

                                    <textarea name="specification" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="userName">Model</label>
                                    <input type="text" name="model" parsley-trigger="change" required
                                           placeholder="Enter Model"  class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="userName">Price</label>
                                    <input type="text" name="price" data-parsley-type="number" parsley-trigger="change"
                                           placeholder="Enter Price"  class="form-control"  >
                                </div>
                                <div class="form-group">
                                    <label for="userName">Chassis Number</label>
                                    <input type="text" name="chassis_number" parsley-trigger="change" required
                                           placeholder="Enter Chassis Number"  class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="userName">Images</label>

                                    <input type="file"  name="image[]"   multiple="true"  >
                                </div>

                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">





                                <div class="form-group">
                                    <label for="emailAddress">Status</label>
                                    <select name="item_status" id="" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>


                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Submit
                                    </button>

                                </div>

                            </form>
                        </div>




                </div><!-- end col -->







            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->












@endsection

@section('script')


    <script type="text/javascript">

        $(document).ready(function() {

            $('select[name="categorie"]').on('change', function() {

                var stateID = $(this).val();

                if(stateID) {

                    $.ajax({

                        url: '/sts/myform/ajax/'+stateID,

                        type: "GET",

                        dataType: "json",

                        success:function(data) {





                            $('select[name="subcategorie"]').empty();

                            $.each(data, function(key, value) {

                                //alert(data);

                                $('select[name="subcategorie"]').append('<option value="'+ value.id +'">'+ value.subcat_name +'</option>');

                            });


                        }

                    });

                }else{

                    $('select[name="subcategorie"]').empty();

                }

            });

        });





    </script>





@endsection