@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>All Avaliable Item</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">






                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">

                                <table id="datatable-buttons" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Model</th>
                                        <th>Picture</th>
                                        <th>Chassis No</th>
                                        <th>Year</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>

                                    <tbody>

                                    @php

                                        $i=1;

                                    @endphp

                                    @foreach($data as $single)
                                        <tr>
                                            <th scope="row">{{$i++}}</th>
                                            <td>
                                                {{$single->item_name}}
                                            </td>
                                            <td>
                                                {{$single->model_no}}
                                            </td>

                                            <td>
                                                @foreach($photo as $pho)

                                                    @if($single->id == $pho->item_id )

                                                        <img src="{{asset("/image/$pho->photo_url")}}" class="thumb-md">

                                                        @break




                                                    @endif

                                                    @endforeach
                                            </td>

                                            <td>

                                                {{$single->chassis_number}}

                                            </td>

                                            <td>

                                                {{$single->year}}

                                            </td>



                                            <td>
                                                <a href="salessingle/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">View</a>

                                                <a href="salesavaliable/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Sales</a>




                                            </td>

                                        </tr>


                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?" +
                "delete Company prodouct");
        });
    </script>

    {{--@include('layouts.datatablejs');--}}


@endsection