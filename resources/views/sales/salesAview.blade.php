@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View Available Item</h2>

@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">






                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">

                            <table id="datatable-buttons" class="table table-striped table-bordered">

                                <tr>
                                    <td>Type</td>
                                    <td>

                                        @foreach($categorie as $cat)

                                            @if($cat->id == $data->categorie)
                                                {{$cat->cat_name}}
                                            @endif

                                        @endforeach

                                    </td>
                                </tr>

                                <tr>
                                    <td width="30%">Item Name</td>
                                    <td>{{$data->item_name}}</td>
                                </tr>

                                <tr>
                                    <td>Year</td>
                                    <td>{{$data->year}}</td>
                                </tr>





                                {{--<tr>--}}
                                    {{--<td>Sub Category</td>--}}
                                    {{--<td>--}}

                                        {{--@foreach($subcategoriess as $sub)--}}
                                            {{--@if($sub->id == $data->subcategorie)--}}
                                                {{--{{$sub->subcat_name}}--}}
                                            {{--@endif--}}
                                        {{--@endforeach--}}

                                    {{--</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>Company</td>--}}
                                    {{--<td>--}}

                                        {{--@foreach($company as $com)--}}
                                            {{--@if($com->id == $data->company_id)--}}

                                                {{--{{$com->company_name}}--}}
                                            {{--@endif--}}

                                            {{--@endforeach--}}

                                    {{--</td>--}}
                                {{--</tr>--}}

                                <tr>
                                    <td>Description</td>
                                    <td><p>{{$data->description}}</p></td>
                                </tr>
                                <tr>
                                    <td>Specification</td>
                                    <td><p>{{$data->specification}}</p></td>
                                </tr>

                                <tr>
                                    <td>Model No</td>
                                    <td>{{$data->model_no}}</td>
                                </tr>


                                <tr>
                                    <td>Chassis Number</td>
                                    <td>{{$data->chassis_number}}</td>
                                </tr>

                                <tr>
                                    <td>Picture</td>
                                    <td>
                                        @foreach($photo as $pho)
                                            <img src="{{asset("/image/$pho->photo_url")}}" width="250">
                                        @endforeach
                                    </td>
                                </tr>



                            </table>

                            <a href="{{route('salesavailable')}}" >Back</a>

                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    {{--@include('layouts.datatablejs');--}}


@endsection