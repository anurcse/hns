<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="wave-lc-management" content="A LC Management Software by Roopokar">
    <meta name="author" content="Roopokar">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/logos/roopokar.png') }}">

    <!-- App title -->
    <title>@yield('title')</title>




    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>



    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            {{--<a href="" class=""><span>Admin<span>to</span></span><i class=""></i></a>--}}

            <div class="user-box">
                <div class="user-img">
                    <img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"></div>
                </div>
                <h5>
                    <a href="#">
                        @if(Auth::check())
                            {{ Auth::user()->name }}
                        @endif
                    </a>
                </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                    {{--<a href="#" class="text-custom">--}}
                    {{--<i class="zmdi zmdi-power"></i>--}}
                    {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="zmdi zmdi-power"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    {{--</ul>--}}
                </ul>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li><h2>Edit Sales</h2></li>
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">@yield('page-header')</h4>
                    </li>
                </ul>



            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">

                <ul class="list-inline">



                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>


                    <li>

                    </li>

                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
        @include('partials.sidebar')
        <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->


        <div class="content">

            <div class="container">






                <!-- end col -->

                <div class="col-lg-12">
                    <div class="card-box p-b-0">

                        @foreach($salesItem as $item)

                        @endforeach





                        <h4 class="header-title m-t-0 m-b-30">Edit Sales</h4>



                       {{--@foreach($salesItem as $item)--}}

                       {{--@endforeach--}}




                        <form id="commentForm" method="post" action="{{route('salesupdate')}}" class="form-horizontal">

                            {{ csrf_field() }}

                            <div id="rootwizard" class="pull-in">
                                <ul>
                                    <li><a href="#first" data-toggle="tab"></a></li>
                                    {{--<li><a href="#second" data-toggle="tab">Customer Info</a></li>--}}
                                    {{--<li><a href="#third" data-toggle="tab">Confirmation </a></li>--}}
                                </ul>
                                <div class="tab-content m-b-0 b-0">
                                    <div class="tab-pane fade" id="first">





                                        <div class="control-group">
                                            <label class="control-label" for="emailfield">Company </label>
                                            <div class="controls">




                                                <select name="company_id" class="form-control" id="">

                                                    @foreach($company as $com)
                                                        <option value="{{$com->id}}" @if($com->id == $item->company_id) selected @endif>{{$com->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>


                                        <div class="control-group">
                                            <label class="control-label" for="emailfield">Category </label>
                                            <div class="controls">
                                                <select name="category_id" class="form-control" id="">


                                                    @foreach($category as $cat)
                                                        <option value="{{$cat->id}}" @if($cat->id == $item->cat_id) selected @endif>{{$cat->cat_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>



                                        <div class="control-group">
                                            <label class="control-label" for="emailfield">Item</label>
                                            <div class="controls">
                                                <select name="item_id" class="form-control" id="">



                                                    @foreach($items3 as $items)
                                                        @if($items->id == $item->item_id)
                                                        <option value="{{$items->id}}">{{$items->item_name}}</option>
                                                            <input type="hidden" value="{{$items->id}}" name="oldItem">
                                                        @endif

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="{{$item->id}}">
                                        <input type="hidden" name="customerId" value="{{$item->customer_id}}">
                                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                                        <div class="control-group">
                                            <label class="control-label" for="emailfield">Sales Price </label>




                                            <div class="controls">
                                                <input class="form-control" id="sales_price" name="sales_price" value="{{$item->sales_price}}" required>
                                            </div>
                                        </div>

                                        {{--@foreach($findCustomer as $cust)--}}


                                            {{--@enforeach()--}}


                                    </div>


                                    <ul class="pager m-b-0 wizard">
                                        {{--<li class="previous"><a href="#" class="btn btn-primary waves-effect waves-light">Previous</a></li>--}}

                                        <li class="next"><button type="submit" class="btn btn-primary waves-effect waves-light  txtAge2 " style=" float: right ">Submit</button></li>

                                    </ul>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- end col -->

            </div>


            <!-- End row -->

        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer">
        2017  - * © Roopokar.
    </footer>

</div>
<!-- End content-page -->




</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- Form wizard -->
<script src="{{asset('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#basicwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

        $('#progressbarwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#progressbarwizard').find('.bar').css({width:$percent+'%'});
        },
            'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

        $('#btnwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted','nextSelector': '.button-next', 'previousSelector': '.button-previous', 'firstSelector': '.button-first', 'lastSelector': '.button-last'});

        var $validator = $("#commentForm").validate({
            rules: {
                emailfield: {
                    required: true,
                    email: true,
                    minlength: 3
                },
                namefield: {
                    required: true,
                    minlength: 3
                },
                urlfield: {
                    required: true,
                    minlength: 3,
                    url: true
                }
            }
        });

        $('#rootwizard').bootstrapWizard({
            'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted',
            'onNext': function (tab, navigation, index) {
                var $valid = $("#commentForm").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }
        });





    });





</script>


<script type="text/javascript">

    $(document).ready(function() {

        $('select[name="company_id"]').on('change', function() {

            var company_id = $(this).val();


            if(company_id) {

                $.ajax({

                    url: '/myform/ajax2/'+company_id,


                    type: "GET",

                    dataType: "json",

                    success:function(data) {







                        $('select[name="item_id"]').empty();

                        $.each(data, function(key, value) {

                            //alert(data);


                            $('select[name="item_id"]').append('<option value="'+ value.id +'">'+ value.item_name +'</option>');

                        });


                    }

                });

            }else{

                $('select[name="item_id"]').empty();

            }

        });

        $('select[name="category_id"]').on('change', function() {


            var category_id = $(this).val();

            var company2 = $('select[name="company_id"]').val();

            if(category_id) {

                $.ajax({

                    // url: '/myform/ajax2/'+category_id/+company2,

                    url: '/sts/myform/ajax2/'+company2+'/'+category_id,






                    type: "GET",

                    dataType: "json",

                    success:function(data) {






                        $('select[name="item_id"]').empty();

                        $.each(data, function(key, value) {

                           // alert(data);

                            $('select[name="item_id"]').append('<option value="'+ value.id +'">'+ value.item_name +'</option>');

                        });


                    }

                });

            }else{

                $('select[name="item_id"]').empty();

            }

        });

    });





</script>


<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<script>

            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"

    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>


</body>
</html>