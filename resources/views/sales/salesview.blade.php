@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View Sales</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">

                @foreach($findSales as $sale )



                @endforeach

                @foreach($findItem as $item)

                @endforeach


                @foreach($findCustomer as $customer)

                @endforeach




                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">

                            <table id="datatable-buttons" class="table table-striped table-bordered">

                                    <tr>
                                        <td colspan="2" width="50%">Item</td>
                                        <td rowspan="11"></td>
                                        <td colspan="2" width="50%">Customer</td>
                                    </tr>



                                <tr>
                                    <td>Type</td>
                                    <td>
                                        @foreach($categorie as $cat)

                                            @if($cat->id == $item->categorie)

                                                {{$cat->cat_name}}

                                            @endif

                                        @endforeach

                                    </td>
                                    <td>Customer</td>
                                    <td>{{$customer->name}}</td>
                                </tr>
                                <tr>
                                    <td>Item Name</td>
                                    <td>{{$item->item_name}}</td>
                                    <td>Address</td>
                                    <td>{{$customer->address}}</td>

                                </tr>

                                {{--<tr>--}}
                                    {{--<td>Sub Category</td>--}}
                                    {{--<td>--}}

                                        {{--@foreach($subcategoriess as $sub)--}}
                                            {{--@if($sub->id == $item->subcategorie)--}}
                                                {{--{{$sub->subcat_name}}--}}
                                                {{--@endif--}}
                                        {{--@endforeach--}}


                                    {{--</td>--}}
                                    {{--<td></td>--}}
                                    {{--<td></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>Company</td>--}}
                                    {{--<td>--}}

                                        {{--@foreach($company as $com)--}}
                                            {{--@if($com->id == $item->company_id)--}}
                                                {{--{{$com->company_name}}--}}
                                                {{--@endif--}}
                                            {{--@endforeach--}}



                                    {{--</td>--}}
                                    {{--<td></td>--}}
                                    {{--<td></td>--}}
                                {{--</tr>--}}
                                <tr>
                                    <td>Year</td>
                                    <td>{{$item->year}}</td>
                                    <td>Phone</td>
                                    <td>{{$customer->phone}}</td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>{{$item->description}}</td>
                                    <td>NID</td>
                                    <td>{{$customer->n_id}}</td>
                                </tr>
                                <tr>
                                    <td>Specification</td>
                                    <td>{{$item->specification}}</td>
                                    <td>TIN ID</td>
                                    <td>{{$customer->tin_id}}</td>
                                </tr>

                                <tr>
                                    <td>Model No</td>
                                    <td>{{$item->model_no}}</td>
                                    <td>REG NO</td>
                                    <td>{{$customer->reg_no}}</td>
                                </tr>

                                <tr>
                                    <td>Chassis Number</td>
                                    <td>{{$item->chassis_number}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Main Picture</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>

                            </table>

                            <a href="{{route('salesindex')}}" >Back</a>

                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    {{--@include('layouts.datatablejs');--}}


@endsection