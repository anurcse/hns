@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View Customer</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



                <?php
                // var_dump($data) or die();
                ?>
                @foreach($data as $singleCustomer)


                @endforeach


                {{--categorie--}}
                {{--subcategoriess--}}
                {{--company--}}
                {{--users--}}

                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">

                            <table id="datatable-buttons" class="table table-striped table-bordered">


                                <tr>
                                    <td>Customer Name</td>
                                    <td>{{$singleCustomer->name}}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{$singleCustomer->email}}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{$singleCustomer->address}}</td>
                                </tr>
                                <tr>
                                    <td>Phone</td>
                                    <td>{{$singleCustomer->phone}}</td>
                                </tr>
                                <tr>
                                    <td>NID</td>
                                    <td>{{$singleCustomer->n_id}}</td>
                                </tr>

                                <tr>
                                    <td>TIN ID</td>
                                    <td>{{$singleCustomer->tin_id}}</td>
                                </tr>
                                <tr>
                                    <td>REG NO</td>
                                    <td>{{$singleCustomer->reg_no}}</td>
                                </tr>

                            </table>

                            <a href="{{route('salesindex')}}" >Back</a>

                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    {{--@include('layouts.datatablejs');--}}


@endsection