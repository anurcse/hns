@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

{{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>All Sales</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">





    @foreach($data as $single)



    @endforeach


            <div class="card-box">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{route('salescreate')}}" class="btn btn-default">Add Sales</a>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">


                            {{--<div class="dropdown pull-right">--}}
                                {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                                    {{--<i class="zmdi zmdi-more-vert"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Action</a></li>--}}
                                    {{--<li><a href="#">Another action</a></li>--}}
                                    {{--<li><a href="#">Something else here</a></li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">Separated link</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}

                            {{--<h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>--}}

                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sales Ref No</th>
                                    <th>Item Name</th>
                                    <th>Customer</th>
                                    <th>Status</th>
                                    <th>commission</th>

                                    <th>Action</th>

                                </tr>
                                </thead>

                                <tbody>

                                @php

                               // var_dump($arr) or die();
                                    $i=1;
                                @endphp
                                @foreach($data as $single)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$single->sales_code}}</td>
                                        <td>
                                            @foreach($items2 as $item)
                                                @if($item->id == $single->item_id) <a href="itemstatus/{{$single->item_id}}">{{ $item->item_name }} </a>  @endif
                                            @endforeach
                                        </td>

                                        <td>

                                            @foreach($allCustomer as $customer)
                                                @if($single->customer_id == $customer->id ) <a href="customerview/{{$customer->id}}">{{$customer->name}}</a>
                                                @endif
                                            @endforeach


                                        </td>

                                        <td>

                                            @if ($single->status == '1')
                                                Active
                                            @else
                                                Inactive
                                            @endif


                                        </td>

                                        <td>

                                            @if ($single->commission_status == '1' && $single->status == '1' )
                                                Paid

                                            @else
                                                Pending
                                            @endif


                                        </td>

                                        <form action="{{route('salesdelete')}}"  method="post">
                                            {{ csrf_field() }}

                                            <input type="hidden"  name="id" value="{{$single->id}}">

                                        <td>

                                            <a href="installmentview/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Installment</a>
                                            {{--<a href="salesstatus/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Sales Status</a>--}}
                                            <a href="salesview/{{$single->id}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Sales View</a>

                                            @if(@$installment2 == NULL)
                                                <a href="salesedit/{{$single->id}}" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</a>

                                            @else
                                                @if(in_array($single->id, @$installment2))


                                                @else

                                                    <a href="salesedit/{{$single->id}}" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</a>

                                                @endif

                                            @endif

                                            @if(Auth::user()->role == 2)
                                                <button type="submit" class="btn btn-danger btn-trans waves-effect w-md waves-danger m-b-5 delete">Delete</button>
                                            @endif

                                        </td>
                                            {{--<td><a href="#" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</a> <button type="submit" class="btn btn-danger btn-trans waves-effect w-md waves-danger m-b-5 delete">Delete</button> </td>--}}

                                        </form>
                                    </tr>


                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>


            </div>





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?" +
                "delete Company prodouct");
        });
    </script>

{{--@include('layouts.datatablejs');--}}


@endsection