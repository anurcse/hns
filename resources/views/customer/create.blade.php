@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />



    <!-- Editatable  Css-->

    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-datatables-editable/datatables.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/toastr/toastr.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/custombox/dist/custombox.min.css') }}">--}}
    <style>
        .required{
            color:red;
        }
        dl{
            padding:15px;
        }
        dd{
            text-align: left;
        }
        .dataTables_length{
            display: none;
        }
    </style>
@endsection

@section('page-header')
    <h2>Create Company</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">

                <div class="col-sm-12">

                        <div class="card-box">


                            <h4 class="header-title m-t-0 m-b-30">Create Company</h4>

                            <form action="{{route('comcreate')}}" data-parsley-validate novalidate method="post">

                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label for="userName">Company Name*</label>
                                    <input type="text" name="company_name" parsley-trigger="change" required
                                           placeholder="Enter Company Name"  class="form-control" >
                                </div>



                                <div class="form-group">
                                    <label for="emailAddress">Status</label>
                                    <select name="company_status" id="" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>


                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Submit
                                    </button>
                                    {{--<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">--}}
                                        {{--Cancel--}}
                                    {{--</button>--}}
                                </div>

                            </form>
                        </div>




                </div><!-- end col -->





                {{--<br>--}}
                {{--<br>--}}


                {{--<form class="form-horizontal"  id="addForm" action="{{ route('create') }}" method="POST">--}}

                    {{--{{ csrf_field() }}--}}
                    {{--<div class="form-group">--}}
                        {{--<label class="col-md-2 control-label">Name</label>--}}
                        {{--<div class="col-md-10">--}}
                            {{--<input type="text" name="name" class="form-control"  required>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<label class="col-md-2 control-label">Roll</label>--}}
                        {{--<div class="col-md-10">--}}
                            {{--<input type="text"  name="roll" class="datepicker form-control" >--}}
                        {{--</div>--}}
                    {{--</div>--}}


                    {{--<div class="modal-footer">--}}
                        {{--<button type="submit" id="addFormSubmit" class="btn btn-primary waves-effect waves-light">Save changes</button>--}}

                    {{--</div>--}}

                {{--</form>--}}

            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->












@endsection

@section('script')
    <!-- Editable js -->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/jquery-datatables-editable/jquery.dataTables.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/tiny-editable/mindmup-editabletable.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/tiny-editable/numeric-input-example.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/toastr/toastr.min.js') }}"></script>--}}

    {{--<script src="{{ asset('/assets/plugins/custombox/dist/custombox.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/custombox/dist/legacy.min.js') }}"></script>--}}

    {{--<!-- init -->--}}
    {{--<script src="{{ asset('/assets/pages/datatables.editable.init.js') }}"></script>--}}






@endsection