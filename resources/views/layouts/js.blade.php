
<!-- jQuery  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/detect.js') }}"></script>
<script src="{{ asset('/assets/js/fastclick.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.blockUI.js') }}"></script>
<script src="{{ asset('/assets/js/waves.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.nicescroll.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.scrollTo.min.js') }}"></script>

<!-- App js -->
<script src="{{ asset('/assets/js/jquery.core.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.app.js') }}"></script>



<!-- Validation js (Parsleyjs) -->
<script type="text/javascript" src="{{ asset('assets/plugins/parsleyjs/dist/parsley.min.js') }}"></script>



<script type="text/javascript">
    $(document).ready(function() {
        $('form').parsley();
    });
</script>


{{--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--}}
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<script>

            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"

    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>