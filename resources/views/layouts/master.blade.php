<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="wave-lc-management" content="A LC Management Software by Roopokar">
    <meta name="author" content="Roopokar">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/logos/roopokar.png') }}">

    <!-- App title -->
    <title>HNS Auto : Inventory and Sales Tracking System : @yield('title')</title>

    @include('layouts.css')
    @include('layouts.datatablecss')
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
    @yield('style')



</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            {{--<a href="" class=""><span>Admin<span>to</span></span><i class=""></i></a>--}}

            <div class="user-box">
                <div class="user-img">
                    <img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"></div>
                </div>
                <h5>
                    <a href="#">
                        @if(Auth::check())
                            {{ Auth::user()->name }}
                        @endif
                    </a>
                </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                    {{--<a href="#" class="text-custom">--}}
                    {{--<i class="zmdi zmdi-power"></i>--}}
                    {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="zmdi zmdi-power"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    {{--</ul>--}}
                </ul>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">@yield('page-header')</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<li>--}}
                        {{--<!-- Notification -->--}}
                        {{--<div class="notification-box">--}}
                            {{--<ul class="list-inline m-b-0">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);" class="right-bar-toggle">--}}
                                        {{--<i class="zmdi zmdi-notifications-none"></i>--}}
                                    {{--</a>--}}
                                    {{--<div class="noti-dot">--}}
                                        {{--<span class="dot"></span>--}}
                                        {{--<span class="pulse"></span>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<!-- End Notification bar -->--}}
                    {{--</li>--}}
                    {{--<li class="hidden-xs">--}}
                        {{--<form role="search" class="app-search">--}}
                            {{--<input type="text" placeholder="Search..."--}}
                                   {{--class="form-control">--}}
                            {{--<a href=""><i class="fa fa-search"></i></a>--}}
                        {{--</form>--}}
                    {{--</li>--}}
                {{--</ul>--}}

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                {{--<div class="user-img">--}}
                    {{--<img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">--}}
                    {{--<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>--}}
                {{--</div>--}}
                {{--<h5>--}}
                    {{--<a href="#">--}}
                        {{--@if(Auth::check())--}}
                            {{--{{ Auth::user()->name }}--}}
                        {{--@endif--}}
                    {{--</a>--}}
                {{--</h5>--}}
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                        {{--<a href="#" class="text-custom">--}}
                            {{--<i class="zmdi zmdi-power"></i>--}}
                        {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                        <li>
                            {{--<a href="{{ route('logout') }}"--}}
                               {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                {{--<i class="zmdi zmdi-power"></i>--}}
                            {{--</a>--}}

                            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</form>--}}
                        </li>
                    {{--</ul>--}}
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            @include('partials.sidebar')
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">

                    @yield('content')

                </div>
                <!-- End row -->

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            2017  - * © Roopokar.
        </footer>

    </div>
    <!-- End content-page -->


    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->
    {{--<div class="side-bar right-bar">--}}
        {{--<a href="javascript:void(0);" class="right-bar-toggle">--}}
            {{--<i class="zmdi zmdi-close-circle-o"></i>--}}
        {{--</a>--}}
        {{--<h4 class="">Notifications</h4>--}}
        {{--<div class="notification-list nicescroll">--}}
            {{--<ul class="list-group list-no-border user-list">--}}
                {{--<li class="list-group-item">--}}
                    {{--<a href="#" class="user-list-item">--}}
                        {{--<div class="avatar">--}}
                            {{--<img src="assets/images/users/avatar-2.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="user-desc">--}}
                            {{--<span class="name">Michael Zenaty</span>--}}
                            {{--<span class="desc">There are new settings available</span>--}}
                            {{--<span class="time">2 hours ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-group-item">--}}
                    {{--<a href="#" class="user-list-item">--}}
                        {{--<div class="icon bg-info">--}}
                            {{--<i class="zmdi zmdi-account"></i>--}}
                        {{--</div>--}}
                        {{--<div class="user-desc">--}}
                            {{--<span class="name">New Signup</span>--}}
                            {{--<span class="desc">There are new settings available</span>--}}
                            {{--<span class="time">5 hours ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-group-item">--}}
                    {{--<a href="#" class="user-list-item">--}}
                        {{--<div class="icon bg-pink">--}}
                            {{--<i class="zmdi zmdi-comment"></i>--}}
                        {{--</div>--}}
                        {{--<div class="user-desc">--}}
                            {{--<span class="name">New Message received</span>--}}
                            {{--<span class="desc">There are new settings available</span>--}}
                            {{--<span class="time">1 day ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-group-item active">--}}
                    {{--<a href="#" class="user-list-item">--}}
                        {{--<div class="avatar">--}}
                            {{--<img src="assets/images/users/avatar-3.jpg" alt="">--}}
                        {{--</div>--}}
                        {{--<div class="user-desc">--}}
                            {{--<span class="name">James Anderson</span>--}}
                            {{--<span class="desc">There are new settings available</span>--}}
                            {{--<span class="time">2 days ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li class="list-group-item active">--}}
                    {{--<a href="#" class="user-list-item">--}}
                        {{--<div class="icon bg-warning">--}}
                            {{--<i class="zmdi zmdi-settings"></i>--}}
                        {{--</div>--}}
                        {{--<div class="user-desc">--}}
                            {{--<span class="name">Settings</span>--}}
                            {{--<span class="desc">There are new settings available</span>--}}
                            {{--<span class="time">1 day ago</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</li>--}}

            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!-- /Right-bar -->

</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

@include('layouts.js')
@include('layouts.datatablejs')

@yield('script')
</body>
</html>