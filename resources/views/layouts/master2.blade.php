<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="wave-lc-management" content="A LC Management Software by Roopokar">
    <meta name="author" content="Roopokar">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/logos/roopokar.png') }}">

    <!-- App title -->
    <title>@yield('title')</title>




    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>


    <!-- App CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="assets/js/modernizr.min.js"></script>



</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            {{--<a href="" class=""><span>Admin<span>to</span></span><i class=""></i></a>--}}

            <div class="user-box">
                <div class="user-img">
                    <img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"></div>
                </div>
                <h5>
                    <a href="#">
                        @if(Auth::check())
                            {{ Auth::user()->name }}
                        @endif
                    </a>
                </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                    {{--<a href="#" class="text-custom">--}}
                    {{--<i class="zmdi zmdi-power"></i>--}}
                    {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="zmdi zmdi-power"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    {{--</ul>--}}
                </ul>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">@yield('page-header')</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
                {{--<ul class="nav navbar-nav navbar-right">--}}
                    {{--<li>--}}
                        {{--<!-- Notification -->--}}
                        {{--<div class="notification-box">--}}
                            {{--<ul class="list-inline m-b-0">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:void(0);" class="right-bar-toggle">--}}
                                        {{--<i class="zmdi zmdi-notifications-none"></i>--}}
                                    {{--</a>--}}
                                    {{--<div class="noti-dot">--}}
                                        {{--<span class="dot"></span>--}}
                                        {{--<span class="pulse"></span>--}}
                                    {{--</div>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<!-- End Notification bar -->--}}
                    {{--</li>--}}
                    {{--<li class="hidden-xs">--}}
                        {{--<form role="search" class="app-search">--}}
                            {{--<input type="text" placeholder="Search..."--}}
                                   {{--class="form-control">--}}
                            {{--<a href=""><i class="fa fa-search"></i></a>--}}
                        {{--</form>--}}
                    {{--</li>--}}
                {{--</ul>--}}

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                {{--<div class="user-img">--}}
                    {{--<img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">--}}
                    {{--<div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>--}}
                {{--</div>--}}
                {{--<h5>--}}
                    {{--<a href="#">--}}
                        {{--@if(Auth::check())--}}
                            {{--{{ Auth::user()->name }}--}}
                        {{--@endif--}}
                    {{--</a>--}}
                {{--</h5>--}}
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                        {{--<a href="#" class="text-custom">--}}
                            {{--<i class="zmdi zmdi-power"></i>--}}
                        {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                        <li>
                            {{--<a href="{{ route('logout') }}"--}}
                               {{--onclick="event.preventDefault();--}}
                                                     {{--document.getElementById('logout-form').submit();">--}}
                                {{--<i class="zmdi zmdi-power"></i>--}}
                            {{--</a>--}}

                            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</form>--}}
                        </li>
                    {{--</ul>--}}
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            @include('partials.sidebar')
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <div class="row">

                    <div class="row">


                        <!-- end col -->

                        <div class="col-lg-6">
                            <div class="card-box p-b-0">
                                <div class="dropdown pull-right">
                                    <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>

                                <h4 class="header-title m-t-0 m-b-30">Wizard With Form Validation</h4>

                                <form id="commentForm" method="get" action="" class="form-horizontal">
                                    <div id="rootwizard" class="pull-in">
                                        <ul>
                                            <li><a href="#first" data-toggle="tab">First</a></li>
                                            <li><a href="#second" data-toggle="tab">Second</a></li>
                                            <li><a href="#third" data-toggle="tab">Third</a></li>
                                        </ul>
                                        <div class="tab-content m-b-0 b-0">
                                            <div class="tab-pane fade" id="first">
                                                <div class="control-group">
                                                    <label class="control-label" for="emailfield">Email</label>
                                                    <div class="controls">
                                                        <input type="text" id="emailfield" name="emailfield"
                                                               class="required email form-control">
                                                    </div>
                                                </div>

                                                <div class="control-group">
                                                    <label class="control-label" for="namefield">Name</label>
                                                    <div class="controls">
                                                        <input type="text" id="namefield" name="namefield"
                                                               class="required form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="second">
                                                <div class="control-group">
                                                    <label class="control-label" for="urlfield">URL</label>
                                                    <div class="controls">
                                                        <input type="text" id="urlfield" name="urlfield"
                                                               class="required url form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="third">
                                                <div class="form-group clearfix">
                                                    <div class="col-lg-12">
                                                        <div class="checkbox checkbox-primary">
                                                            <input id="checkbox-h3" type="checkbox">
                                                            <label for="checkbox-h3">
                                                                I agree with the Terms and Conditions.
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <ul class="pager m-b-0 wizard">
                                                <li class="previous"><a href="#" class="btn btn-primary waves-effect waves-light">Previous</a></li>
                                                <li class="next"><a href="#" class="btn btn-primary waves-effect waves-light">Next</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <!-- end col -->

                    </div>

                </div>
                <!-- End row -->

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer">
            2017  - * © Roopokar.
        </footer>

    </div>
    <!-- End content-page -->




</div>
<!-- END wrapper -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/detect.js"></script>
<script src="assets/js/fastclick.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/jquery.blockUI.js"></script>
<script src="assets/js/waves.js"></script>
<script src="assets/js/jquery.nicescroll.js"></script>
<script src="assets/js/jquery.scrollTo.min.js"></script>

<!-- Form wizard -->
<script src="assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.js"></script>
<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>

<!-- App js -->
<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#basicwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

        $('#progressbarwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#progressbarwizard').find('.bar').css({width:$percent+'%'});
        },
            'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted'});

        $('#btnwizard').bootstrapWizard({'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted','nextSelector': '.button-next', 'previousSelector': '.button-previous', 'firstSelector': '.button-first', 'lastSelector': '.button-last'});

        var $validator = $("#commentForm").validate({
            rules: {
                emailfield: {
                    required: true,
                    email: true,
                    minlength: 3
                },
                namefield: {
                    required: true,
                    minlength: 3
                },
                urlfield: {
                    required: true,
                    minlength: 3,
                    url: true
                }
            }
        });

        $('#rootwizard').bootstrapWizard({
            'tabClass': 'nav nav-tabs navtab-wizard nav-justified bg-muted',
            'onNext': function (tab, navigation, index) {
                var $valid = $("#commentForm").valid();
                if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                }
            }
        });
    });

</script>
</body>
</html>