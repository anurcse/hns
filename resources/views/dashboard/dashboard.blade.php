@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Editatable  Css-->

    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/magnific-popup/dist/magnific-popup.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/jquery-datatables-editable/datatables.css') }}" />--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/toastr/toastr.min.css') }}">--}}
    {{--<link rel="stylesheet" href="{{ asset('/assets/plugins/custombox/dist/custombox.min.css') }}">--}}
    <style>
        .required{
            color:red;
        }
        dl{
            padding:15px;
        }
        dd{
            text-align: left;
        }
        .dataTables_length{
            display: none;
        }
    </style>
@endsection

@section('page-header')
    <h2>Dashboard</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">

                <div class="row">

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Total User</h4>

                            {{--<div class="widget-box-2">--}}
                                <div class="widget-detail-2">
                                    {{--<span class="badge badge-success pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span>--}}
                                    <h2 class="m-b-0"> {{$userCount}} </h2>
                                    <p class="text-muted m-b-25">Total User</p>
                                </div>
                                {{--<div class="progress progress-bar-success-alt progress-sm m-b-0">--}}
                                    {{--<div class="progress-bar progress-bar-success" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div><!-- end col -->

                    <div class="col-lg-3 col-md-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Available Item</h4>

                            {{--<div class="widget-box-2">--}}
                                <div class="widget-detail-2">
                                    {{--<span class="badge badge-success pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span>--}}
                                    <h2 class="m-b-0"> {{$AvailableItem}} </h2>
                                    <p class="text-muted m-b-25">Available Item</p>
                                </div>
                                {{--<div class="progress progress-bar-success-alt progress-sm m-b-0">--}}
                                    {{--<div class="progress-bar progress-bar-success" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div><!-- end col -->


                    <div class="col-lg-3 col-md-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Total Sales</h4>

                            {{--<div class="widget-box-2">--}}
                                <div class="widget-detail-2">
                                    {{--<span class="badge badge-success pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span>--}}
                                    <h2 class="m-b-0"> {{$tsales}} </h2>
                                    <p class="text-muted m-b-25">Total Sales</p>
                                </div>
                                {{--<div class="progress progress-bar-success-alt progress-sm m-b-0">--}}
                                    {{--<div class="progress-bar progress-bar-success" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div><!-- end col -->



                    <div class="col-lg-3 col-md-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Daily Sales</h4>

                            {{--<div class="widget-box-2">--}}
                                <div class="widget-detail-2">
                                    {{--<span class="badge badge-pink pull-left m-t-20">32% <i class="zmdi zmdi-trending-up"></i> </span>--}}
                                    <h2 class="m-b-0">  {{$sumtodaysales}}  </h2>
                                    <p class="text-muted m-b-25">Daily Sales</p>
                                </div>
                                {{--<div class="progress progress-bar-pink-alt progress-sm m-b-0">--}}
                                    {{--<div class="progress-bar progress-bar-pink" role="progressbar"--}}
                                         {{--aria-valuenow="77" aria-valuemin="0" aria-valuemax="100"--}}
                                         {{--style="width: 77%;">--}}
                                        {{--<span class="sr-only">77% Complete</span>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                    </div><!-- end col -->

                </div>
                <!-- end row -->

            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->












@endsection

@section('script')
    <!-- Editable js -->
    {{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/magnific-popup/dist/jquery.magnific-popup.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/jquery-datatables-editable/jquery.dataTables.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/tiny-editable/mindmup-editabletable.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/tiny-editable/numeric-input-example.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/toastr/toastr.min.js') }}"></script>--}}

    {{--<script src="{{ asset('/assets/plugins/custombox/dist/custombox.min.js') }}"></script>--}}
    {{--<script src="{{ asset('/assets/plugins/custombox/dist/legacy.min.js') }}"></script>--}}

    {{--<!-- init -->--}}
    {{--<script src="{{ asset('/assets/pages/datatables.editable.init.js') }}"></script>--}}

    <script>

    </script>
@endsection