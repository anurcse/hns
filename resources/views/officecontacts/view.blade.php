@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View Contacts</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



                <?php
                // var_dump($data) or die();
                ?>
                @foreach($data as $singleUser)


                @endforeach


                {{--categorie--}}
                {{--subcategoriess--}}
                {{--company--}}
                {{--users--}}

                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">

                            <table id="datatable-buttons" class="table table-striped table-bordered">

                                <tr>
                                    <td>ID</td>
                                    <td>{{$singleUser->id}}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{{$singleUser->name }}</td>
                                </tr>

                                <tr>
                                    <td>Company</td>
                                    <td>
                                        @foreach($company as $com)
                                            @if($singleUser->company_id == $com->id)
                                                 {{$com->company_name}}
                                            @endif
                                        @endforeach
                                    </td>
                                </tr>



                                <tr>
                                    <td>Phone</td>
                                    <td>{{$singleUser->phone1}}</td>
                                </tr>

                                <tr>
                                    <td>Phone</td>
                                    <td>{{$singleUser->phone2}}</td>
                                </tr>



                                <tr>
                                    <td>Email</td>
                                    <td>{{$singleUser->email}}</td>
                                </tr>

                                <tr>
                                    <td>Address</td>
                                    <td>{{$singleUser->address}}</td>
                                </tr>

                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($singleUser->status == '1')
                                            Active
                                            @else
                                            Inactive
                                        @endif
                                        </td>
                                </tr>




                            </table>

                            <a href="{{route('officecontactindex')}}" >Back</a>

                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    {{--@include('layouts.datatablejs');--}}


@endsection