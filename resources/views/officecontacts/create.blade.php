<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="wave-lc-management" content="A LC Management Software by Roopokar">
    <meta name="author" content="Roopokar">
    <!-- App Favicon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/logos/roopokar.png') }}">

    <!-- App title -->
    <title>@yield('title')</title>




    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>



    <link href="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/multiselect/css/multi-select.css')}}"  rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/plugins/select2/dist/css/select2.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/select2/dist/css/select2-bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet">

    <!-- App CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/core.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/components.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/pages.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/menu.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/responsive.css')}}" rel="stylesheet" type="text/css" />



    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="{{asset('assets/js/modernizr.min.js')}}"></script>


    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">



</head>


<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">
        <!-- LOGO -->
        <div class="topbar-left">
            {{--<a href="" class=""><span>Admin<span>to</span></span><i class=""></i></a>--}}

            <div class="user-box">
                <div class="user-img">
                    <img src="{{ asset('/assets/images/users/avatar-1.jpg') }}" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status offline"></div>
                </div>
                <h5>
                    <a href="#">
                        @if(Auth::check())
                            {{ Auth::user()->name }}
                        @endif
                    </a>
                </h5>
                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>

                    {{--<li>--}}
                    {{--<a href="#" class="text-custom">--}}
                    {{--<i class="zmdi zmdi-power"></i>--}}
                    {{--</a>--}}


                    {{--</li>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            <i class="zmdi zmdi-power"></i>
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                    {{--</ul>--}}
                </ul>
            </div>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li><h2>Create Contacts</h2></li>
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">@yield('page-header')</h4>
                    </li>
                </ul>



            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">

                <ul class="list-inline">
                    <li>
                        <a href="#" >
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>


                    <li>

                    </li>

                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
        @include('partials.sidebar')
        <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->



    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->


        <div class="content">

            <div class="container">






                <!-- end col -->

                <div class="col-lg-12">
                    <div class="card-box p-b-0">


                        <h4 class="header-title m-t-0 m-b-30">Create Contacts</h4>



                        <form action="{{route('officecontactcreate')}}" data-parsley-validate novalidate method="post">

                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="userName">Name*</label>
                                <input type="text" name="name" parsley-trigger="change" required
                                       placeholder="Enter Name"  class="form-control" minlength="3" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Address</label>
                                <textarea name="address" class="form-control" required></textarea>
                            </div>


                            {{--<div class="form-group">--}}
                                {{--<label >Birthday</label>--}}

                                {{--<div class="input-group">--}}
                                    {{--<input type="text" name="birthday" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">--}}
                                    {{--<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>--}}
                                {{--</div><!-- input-group -->--}}

                            {{--</div>--}}


                            <div class="form-group">
                                <label for="userName">Contact Person*</label>
                                <input type="text" name="contact_person" parsley-trigger="change" required
                                       placeholder="Enter User Name"  class="form-control" minlength="3" >
                            </div>



                            <div class="form-group">
                                <label for="userName">Phone 1*</label>
                                <input type="text" name="phone1" parsley-trigger="change" required
                                       placeholder="Enter Phone Number"  class="form-control"  data-parsley-type="number" minlength="11" >
                            </div>

                            <div class="form-group">
                                <label for="userName">Phone 2</label>
                                <input type="text" name="phone2" parsley-trigger="change"
                                       placeholder="Enter Phone Number"  class="form-control" data-parsley-type="number" minlength="11" >
                            </div>
                            <div class="form-group">
                                <label for="userName">Email*</label>
                                <input type="email" name="email" parsley-trigger="change" required
                                       placeholder="Enter Email Name"  class="form-control" parsley-type="email" >
                            </div>


                            <div class="form-group">
                                <label for="userName">Description</label>

                                <textarea name="description" class="form-control"></textarea>

                            </div>





                            <div class="form-group">
                                <label for="emailAddress">Company</label>
                                <select name="company_id" id="" class="form-control">

                                    @foreach($company as $com)
                                        <option value="{{$com->id}}">{{$com->company_name}}</option>
                                    @endforeach
                                </select>
                            </div>






                            <div class="form-group">
                                <label for="emailAddress">Status</label>
                                <select name="status" id="" class="form-control">
                                    <option value="1">Active</option>
                                    <option value="0">Inactive</option>
                                </select>
                            </div>


                            <div class="form-group text-right m-b-0">
                                <button class="btn btn-primary waves-effect waves-light" type="submit">
                                    Submit
                                </button>
                                {{--<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">--}}
                                {{--Cancel--}}
                                {{--</button>--}}
                            </div>

                        </form>
                    </div>
                </div>
                <!-- end col -->

            </div>


            <!-- End row -->

        </div> <!-- container -->

    </div> <!-- content -->

    <footer class="footer">
        2017  - * © Roopokar.
    </footer>

</div>
<!-- End content-page -->




</div>
<!-- END wrapper -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/js/detect.js')}}"></script>
<script src="{{asset('assets/js/fastclick.js')}}"></script>
<script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{asset('assets/js/waves.js')}}"></script>
<script src="{{asset('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{asset('assets/js/jquery.scrollTo.min.js')}}"></script>

<!-- Plugins Js -->
<script src="{{asset('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/multiselect/js/jquery.multi-select.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/plugins/jquery-quicksearch/jquery.quicksearch.js')}}"></script>
<script src="{{asset('assets/plugins/select2/dist/js/select2.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/plugins/moment/moment.js')}}"></script>
<script src="{{asset('assets/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}" type="text/javascript"></script>

<!-- App js -->
<script src="{{asset('assets/js/jquery.core.js')}}"></script>
<script src="{{asset('assets/js/jquery.app.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/plugins/parsleyjs/dist/parsley.min.js') }}"></script>
<script>
    jQuery(document).ready(function() {

        //advance multiselect start
        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

        // Select2
        $(".select2").select2();

        $(".select2-limiting").select2({
            maximumSelectionLength: 2
        });

    });

    //Bootstrap-TouchSpin
    $(".vertical-spin").TouchSpin({
        verticalbuttons: true,
        buttondown_class: "btn btn-primary",
        buttonup_class: "btn btn-primary",
        verticalupclass: 'ti-plus',
        verticaldownclass: 'ti-minus'
    });
    var vspinTrue = $(".vertical-spin").TouchSpin({
        verticalbuttons: true
    });
    if (vspinTrue) {
        $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
    }


    // Date Picker
    jQuery('#datepicker').datepicker();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });
    jQuery('#datepicker-inline').datepicker();
    jQuery('#datepicker-multiple-date').datepicker({
        format: "mm/dd/yyyy",
        clearBtn: true,
        multidate: true,
        multidateSeparator: ","
    });
    jQuery('#date-range').datepicker({
        toggleActive: true
    });

//    //Date range picker
//    $('.input-daterange-datepicker').daterangepicker({
//        buttonClasses: ['btn', 'btn-sm'],
//        applyClass: 'btn-default',
//        cancelClass: 'btn-primary'
//    });
//    $('.input-daterange-timepicker').daterangepicker({
//        timePicker: true,
//        format: 'MM/DD/YYYY h:mm A',
//        timePickerIncrement: 30,
//        timePicker12Hour: true,
//        timePickerSeconds: false,
//        buttonClasses: ['btn', 'btn-sm'],
//        applyClass: 'btn-default',
//        cancelClass: 'btn-primary'
//    });
//    $('.input-limit-datepicker').daterangepicker({
//        format: 'MM/DD/YYYY',
//        minDate: '06/01/2016',
//        maxDate: '06/30/2016',
//        buttonClasses: ['btn', 'btn-sm'],
//        applyClass: 'btn-default',
//        cancelClass: 'btn-primary',
//        dateLimit: {
//            days: 6
//        }
//    });

    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

//    $('#reportrange').daterangepicker({
//        format: 'MM/DD/YYYY',
//        startDate: moment().subtract(29, 'days'),
//        endDate: moment(),
//        minDate: '01/01/2016',
//        maxDate: '12/31/2016',
//        dateLimit: {
//            days: 60
//        },
//        showDropdowns: true,
//        showWeekNumbers: true,
//        timePicker: false,
//        timePickerIncrement: 1,
//        timePicker12Hour: true,
//        ranges: {
//            'Today': [moment(), moment()],
//            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
//            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
//            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
//            'This Month': [moment().startOf('month'), moment().endOf('month')],
//            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
//        },
//        opens: 'left',
//        drops: 'down',
//        buttonClasses: ['btn', 'btn-sm'],
//        applyClass: 'btn-success',
//        cancelClass: 'btn-default',
//        separator: ' to ',
//        locale: {
//            applyLabel: 'Submit',
//            cancelLabel: 'Cancel',
//            fromLabel: 'From',
//            toLabel: 'To',
//            customRangeLabel: 'Custom',
//            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
//            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
//            firstDay: 1
//        }
//    }, function (start, end, label) {
//        console.log(start.toISOString(), end.toISOString(), label);
//        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
//    });

    //Bootstrap-MaxLength
    $('input#defaultconfig').maxlength()

    $('input#thresholdconfig').maxlength({
        threshold: 20
    });

    $('input#moreoptions').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger"
    });

    $('input#alloptions').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: ' out of ',
        preText: 'You typed ',
        postText: ' chars available.',
        validate: true
    });

    $('textarea#textarea').maxlength({
        alwaysShow: true
    });

    $('input#placement').maxlength({
        alwaysShow: true,
        placement: 'top-left'
    });
</script>





<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



<script>

            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"

    switch(type){
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>


</body>
</html>