
@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

{{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>All Installment</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



    {{--@foreach($data as $single)--}}



    {{--@endforeach--}}


            <div class="card-box">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            {{--<div class="dropdown pull-right">--}}
                                {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                                    {{--<i class="zmdi zmdi-more-vert"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Action</a></li>--}}
                                    {{--<li><a href="#">Another action</a></li>--}}
                                    {{--<li><a href="#">Something else here</a></li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">Separated link</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}




                            <?php
                            foreach($data as $single){

                               @$instalMent += $single->amount;

                            }


                            foreach($allSalse as $salse){




                                if($salse->id == @$single->sales_id){
                                    $saleCom = $salse->sales_price;

                                    back();
                                }


                            }

//                            echo $instalMent;
//

                            if(@$instalMent < @$saleCom ){
                                ?>
                                <a href="/sts/installmentrcreate/{{$iditem}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Create</a>
                            <?php
                            }else{
                            ?>
                            <a href="/sts/installmentrcreate/{{$iditem}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Create</a>
                            <?php
                            }



                            ?>

                            {{--<h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>--}}

                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <td>Sale Ref No</td>
                                    <th>Name</th>
                                    <th>Amount</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                                <tbody>

                                @php
                                    $i=1;
                                @endphp
                                @foreach($data as $single)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>

                                            @foreach($allSalse as $salse)
                                                    @if($salse->id == $single->sales_id)
                                                    {{$salse->sales_code}}
                                                        
                                                             
                                                @endif
                                                            
                                                    
                                            @endforeach


                                            </td>
                                        <td>

                                            @foreach($allSalse as $salse)
                                                    @if($salse->id == $single->sales_id)
                                                        @foreach($items2 as $item)
                                                            @if($item->id == $salse->item_id )
                                                                {{$item->item_name}}
                                                                @endif
                                                            @endforeach
                                                    @endif
                                            @endforeach


                                            </td>
                                        <td>
                                            {{$single->amount}}



                                        </td>


                                        <td>


                                        {{--<form action="{{route('comdelete')}}"  method="post">--}}
                                            {{--{{ csrf_field() }}--}}

                                            {{--<input type="hidden"  name="id" value="{{$single->id}}">--}}

                                        {{--<a href="/installmentedit/{{$single->id}}" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</a> <button type="submit" class="btn btn-danger btn-trans waves-effect w-md waves-danger m-b-5 delete">Delete</button>--}}
                                        {{--</form>--}}

                                        <form action="{{route('installmentview')}}"  method="post">
                                                {{ csrf_field() }}

                                            <input type="hidden"  name="installment_id" value="{{$single->id}}">
                                            <input type="hidden"  name="sales_id" value="{{$single->sales_id}}">


                                            <button type="submit" class="btn btn-warning btn-trans waves-effect w-md waves-warning m-b-5">Edit</button>
                                        </form>

                                            <form action="{{route('installmentdelete')}}"  method="post">
                                                {{ csrf_field() }}

                                                <input type="hidden"  name="id" value="{{$single->id}}">

                                                <button type="submit" class="btn btn-danger btn-trans waves-effect w-md waves-danger m-b-5 delete">Delete</button>
                                            </form>

                                        </td>
                                    </tr>



                                    @php @$sum += $single->amount @endphp

                                @endforeach



                                </tbody>

                                <tr>

                                    <td colspan="2">Total</td>
                                    <td colspan="2">{{@$sum}}</td>

                                </tr>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>


            </div>





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?" +
                "delete Company prodouct");
        });
    </script>

{{--@include('layouts.datatablejs');--}}


@endsection