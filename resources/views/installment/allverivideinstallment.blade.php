
@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

{{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>All Verified </h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



    {{--@foreach($data as $single)--}}



    {{--@endforeach--}}


            <div class="card-box">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            {{--<div class="dropdown pull-right">--}}
                                {{--<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">--}}
                                    {{--<i class="zmdi zmdi-more-vert"></i>--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu" role="menu">--}}
                                    {{--<li><a href="#">Action</a></li>--}}
                                    {{--<li><a href="#">Another action</a></li>--}}
                                    {{--<li><a href="#">Something else here</a></li>--}}
                                    {{--<li class="divider"></li>--}}
                                    {{--<li><a href="#">Separated link</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}




                            <?php
                            foreach($data as $single){

                               @$instalMent += $single->amount;

                            }

                            ?>






                                {{--<a href="/installmentrcreate/{{$iditem}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Create</a>--}}

                            {{--<a href="/installmentrcreate/{{$iditem}}" class="btn btn-info btn-trans waves-effect w-md waves-info m-b-5">Create</a>--}}






                            {{--<h4 class="header-title m-t-0 m-b-30">Buttons Example</h4>--}}

                            <table id="datatable-buttons" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Sales Ref No</th>
                                    <th>Amount</th>
                                    <th>Status</th>
                                    {{--<th>Action</th>--}}

                                </tr>
                                </thead>

                                <tbody>

                                @foreach($data as $da)

                                    <tr>
                                        <td>{{$da->id}}</td>
                                       
                                       <td>

                                            @foreach($allSalse as $salse)
                                                    @if($salse->id == $da->id)
                                                    {{$salse->sales_code}}
                                                        
                                                             
                                                @endif
                                                            
                                                    
                                            @endforeach


                                            </td>
                                        <td>{{$da->amount}}</td>
                                        <td>@if($da->installment_verified == 1) Verified @else Unverified @endif</td>
                                        {{--<td><a href="installmentverified/{{$da->id}}" class="btn btn-default btn-trans waves-effect w-md waves-default m-b-5">Change Status</a></td>--}}
                                    </tr>

                                @endforeach



                                </tbody>


                            </table>
                        </div>
                    </div><!-- end col -->
                </div>

            </div>


            </div>





            </div> <!-- end panel -->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    <script>
        $(".delete").on("click", function(){
            return confirm("Do you want to delete this item?" +
                "delete Company prodouct");
        });
    </script>

{{--@include('layouts.datatablejs');--}}


@endsection