@extends('layouts.master')

@section('title','Dashboard')

@section('style')

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    {{--@include('layouts.datatablecss')--}}

@endsection

@section('page-header')
    <h2>View User</h2>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="panel">



                <?php
                // var_dump($data) or die();
                ?>
                @foreach($data as $singleUser)


                @endforeach


                {{--categorie--}}
                {{--subcategoriess--}}
                {{--company--}}
                {{--users--}}

                <div class="card-box">

                    <div class="row">
                        <div class="col-sm-12">

                            <table id="datatable-buttons" class="table table-striped table-bordered">

                                <tr>
                                    <td>Name</td>
                                    <td>{{$singleUser->first_name }} {{ $singleUser->last_name }}</td>
                                </tr>

                                <tr>
                                    <td>Email</td>
                                    <td>{{$singleUser->email}}</td>
                                </tr>

                                <tr>
                                    <td>Gender</td>
                                    <td>
                                        @if($singleUser->gender == 1)
                                            Male
                                        @else
                                            Female
                                        @endif
                                    </td>
                                </tr>

                                <tr>
                                    <td>Birthday</td>
                                    <td>

                                        <?php echo date('m/d/Y', strtotime( $singleUser->birthday));?>
                                        </td>
                                </tr>

                                <tr>
                                    <td>Role</td>
                                    <td>
                                        @if($singleUser->role == '1')
                                            Admin
                                        @elseif($singleUser->role == '2')
                                                Admin2
                                        @elseif($singleUser->role == '3')
                                                    Admin3
                                        @elseif($singleUser->role == '4')
                                            user
                                        @endif

                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($singleUser->status == '1')
                                            Active
                                            @else
                                            Inactive
                                        @endif
                                        </td>
                                </tr>




                            </table>

                            <a href="{{route('userindex')}}" >Back</a>

                        </div><!-- end col -->
                    </div>

                </div>


            </div>





        </div> <!-- end panel -->
    </div> <!-- end col-->
    </div>
    <!-- end row -->

@endsection

@section('script')

    {{--@include('layouts.datatablejs');--}}


@endsection