<div id="sidebar-menu">
    <ul>
        <li class="text-muted menu-title"></li>

        <li>
            <a href="" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
        </li>

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-file-o" aria-hidden="true"></i> <span> Category </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('catcreate')}}">Add Category</a></li>
                <li><a href="{{route('catindex')}}">View Category </a></li>

            </ul>
        </li>
        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-files-o" aria-hidden="true"></i> <span> Sub Category </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('subcatcreate')}}">Add Sub Category</a></li>
                <li><a href="{{route('subcatindex')}}">View Sub Category </a></li>

            </ul>
        </li>

        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-building" aria-hidden="true"></i> <span> Company </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('comcreate')}}">Add Company</a></li>
                <li><a href="{{route('comindex')}}">View Company </a></li>

            </ul>
        </li>

        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-car" aria-hidden="true"></i> <span> Item </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('itemcreate')}}">Add Item</a></li>
                <li><a href="{{route('itemindex')}}">View Item </a></li>
                <li><a href="{{route('salesavailable')}}">Available item</a></li>

            </ul>
        </li>
        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)
            <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Installment</span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                    <li><a href="{{route('installmentindex')}}">All Installment</a></li>
                    <li><a href="{{route('verifiedinstallment')}}">Verified Installment </a></li>
                    <li><a href="{{route('unverifiedinstallment')}}">Unverified Installment</a></li>

                </ul>
            </li>
        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 4)

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-money" aria-hidden="true"></i> <span> Sales </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('salescreate')}}">Add Sales</a></li>
                @if(Auth::user()->role == 1 || Auth::user()->role == 2)
                <li><a href="{{route('salesindex')}}">All Sales</a></li>
                @endif
                @if(Auth::user()->role == 4)
                <li><a href="{{route('salesindexuser')}}">All Sales</a></li>
                @endif
                <li><a href="{{route('salesavailable')}}">Available item</a></li>




            </ul>
        </li>

        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2 )
        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-user" aria-hidden="true"></i> <span> User </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('usercreate')}}">Add User</a></li>
                <li><a href="{{route('userindex')}}">All User</a></li>

            </ul>
        </li>

        @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-phone" aria-hidden="true"></i> <span> Contact </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('officecontactcreate')}}">Add Contact</a></li>
                <li><a href="{{route('officecontactindex')}}">All Contact</a></li>


            </ul>
        </li>

            @endif

        @if(Auth::user()->role == 1 || Auth::user()->role == 2)

        <li class="has_sub">
            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-users" aria-hidden="true"></i><span> Customer </span> <span class="menu-arrow"></span></a>
            <ul class="list-unstyled">
                <li><a href="{{route('customerindex')}}">All Customer</a></li>
                


            </ul>
        </li>

            @endif
            
            
            
            





    </ul>
    <div class="clearfix"></div>
</div>