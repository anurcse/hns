<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('categorie')->unsigned();
            $table->foreign('categorie')->references('id')->on('categories');
            $table->integer('subcategorie')->unsigned();
            $table->foreign('subcategorie')->references('id')->on('sub_categories');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->string('item_name');
            $table->string('year');
            $table->string('description');
            $table->string('specification');
            $table->string('model_no');
            $table->string('price');
            $table->string('chassis_number')->unique();
            //$table->string('logo');
            //$table->string('photo_id');
            $table->string('item_status');
            $table->string('user_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
