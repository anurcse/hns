<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstallmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sales_id')->unsigned();
            $table->foreign('sales_id')->references('id')->on('sales');
            $table->integer('type');
            $table->tinyInteger('collection_status');
            $table->string('payment_method');
            $table->integer('amount');
            $table->date('installment_date');
            $table->string('check_no');
            $table->string('bank_name');
            $table->string('branch_name');
            $table->tinyInteger('installment_verified');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installments');
    }
}
