<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.master');
// });

//Route::get('/', function () {
//    //return view('test.test');
//
//    return view('');
//})->middleware('auth');



Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');;




////////////////////////// Start Categorie Route ///////////////////////////////

Route::get('/catindex','CategorieController@index')->name('catindex');
Route::get('/catcreate','CategorieController@create')->name('catcreate');
Route::post('/catcreate','CategorieController@store')->name('catcreate');
Route::get('/catview/{id}','CategorieController@show')->name('catview');
Route::get('/catedit/{id}','CategorieController@edit')->name('catedit');
Route::post('/catupdate','CategorieController@update')->name('catupdate');
Route::post('/catdelete','CategorieController@destroy')->name('catdelete');







//////////////////////////// End  Categorie Route //////////////////////////////






////////////////////////////// Start Sub Categorie Route ////////////////////////////


Route::get('/subcatindex','SubCategorieController@index')->name('subcatindex');
Route::get('/subcatcreate','SubCategorieController@create')->name('subcatcreate');
Route::post('/subcatcreate','SubCategorieController@store')->name('subcatcreate');
Route::get('/subcatview/{id}','SubCategorieController@show')->name('subcatview');
Route::get('/subcatedit/{id}','SubCategorieController@edit')->name('subcatedit');
Route::post('/subcatupdate','SubCategorieController@update')->name('subcatupdate');
Route::post('/subcatdelete','SubCategorieController@destroy')->name('subcatdelete');



////////////////////////////// End Sub Categorie Route ////////////////////////////



///////////////////////// Start Company Route/////////////////////////////



Route::get('/comindex','CompanyController@index')->name('comindex');
Route::get('/comcreate','CompanyController@create')->name('comcreate');
Route::post('/comcreate','CompanyController@store')->name('comcreate');
Route::get('/comview/{id}','CompanyController@show')->name('comview');
Route::get('/comedit/{id}','CompanyController@edit')->name('comedit');
Route::post('/comupdate','CompanyController@update')->name('comupdate');
Route::post('/comdelete','CompanyController@destroy')->name('comdelete');


////////////////////////// End Company Route ////////////////////////////


/////////////////////////// Start Item Route ///////////////////////////


Route::get('/itemindex','ItemController@index')->name('itemindex');
//Route::get('/icreate','ItemController@create')->name('icreate');
Route::post('/itemcreate','ItemController@store')->name('itemcreate');
Route::get('/itemview/{id}','ItemController@show')->name('itemview');
Route::get('/itemedit/{id}','ItemController@edit')->name('itemedit');
Route::post('/itemupdate','ItemController@update')->name('itemupdate');
Route::post('/itemdelete','ItemController@destroy')->name('itemdelete');


Route::get('/itemstatus/{id}','ItemController@itemstatus')->name('itemstatus');

Route::post('/itemstatus/','ItemController@updateitemstatus')->name('itemstatus');




Route::get('/itemcreate',array('as'=>'myform','uses'=>'ItemController@myform'));

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'ItemController@myformAjax'));

Route::get('itemphotodel/{id}','ItemController@delephoto')->name('itemphotodel');


/////////////////////////// End Item Route ///////////////////////////


///////////////////////// Start Sales Route ////////////////////////


Route::get('/salesindex','SalesController@index')->name('salesindex');

Route::get('/salesindexuser','SalesController@index2')->name('salesindexuser');

Route::get('/salesavaliable/{id}','SalesController@salesToAvaliable')->name('salesavaliable');



//Route::get('/salescreate','SalesController@create')->name('salescreate');
Route::post('/salescreate','SalesController@store')->name('salescreate');
Route::get('/salesview/{id}','SalesController@show')->name('salesview');
Route::get('/salesedit/{id}','SalesController@edit')->name('salesedit');
//Route::post('/salesupdate','SalesController@update')->name('salesupdate');
Route::post('/salesupdate','SalesController@salesupdate')->name('salesupdate');

Route::post('/salesdelete','SalesController@destroy')->name('salesdelete');

Route::get('/salesstatus/{id}','SalesController@SalesStatus')->name('salesstatus');
Route::post('/salesstatus','SalesController@SalesStatusUpdate')->name('salesstatus');
Route::get('/salesavailable','SalesController@salesAvailable')->name('salesavailable');

//Route::get('/salessingle/{id}','SalesController@salesaview')->name('salesavailable');

Route::get('/salessingle/{id}','SalesController@salessingle')->name('salessingle');








Route::get('/salescreate',array('as'=>'myform','uses'=>'SalesController@myform'));

Route::get('myform/ajax2/{id}',array('as'=>'myform.ajax2','uses'=>'SalesController@myformAjax'));
Route::get('myform/ajax2/{id}/{id2}',array('as'=>'myform.ajax2','uses'=>'SalesController@myformAjax2'));





///////////////////////// End Sales Route ////////////////////////


////////////////////////// User Route ////////////////////////////






Route::get('/userindex','UserControler@index')->name('userindex');
Route::get('/usercreate','UserControler@create')->name('usercreate');
Route::post('/usercreate','UserControler@store')->name('usercreate');
Route::get('/userview/{id}','UserControler@show')->name('userview');
Route::get('/useredit/{id}','UserControler@edit')->name('useredit');
Route::post('/userupdate','UserControler@update')->name('userupdate');
Route::post('/userdelete','UserControler@destroy')->name('userdelete');

Route::get('/changepass/{id}','UserControler@userChangePassword')->name('userchangepassword');

Route::post('/changepassupdate','UserControler@userPasswordUpdate')->name('changepassupdate');













/////////////////////////////End User Route /////////////////////



////////////////Customer Route////////////////////
///



Route::get('/customerindex','CustomerController@index')->name('customerindex');
Route::get('/customerrcreate','CustomerController@create')->name('customercreate');
Route::post('/customercreate','CustomerController@store')->name('customercreate');
Route::get('/customerview/{id}','CustomerController@show')->name('customerview');
Route::get('/customeredit/{id}','CustomerController@edit')->name('customeredit');
Route::post('/customerupdate','CustomerController@update')->name('customerupdate');
Route::post('/customerdelete','CustomerController@destroy')->name('customerdelete');


/////////////////end coustomer Route//////////////////////////
///
///



////////////////Customer Route////////////////////
///



Route::get('/installmentindex','InstallmentController@index')->name('installmentindex');
Route::get('/installmentverified/{id}','InstallmentController@Verified')->name('installmentverified');

Route::post('/changestatus','InstallmentController@VerifiedStore')->name('changestatus');

Route::get('/verifiedinstallment','InstallmentController@VerifiedInstallMent')->name('verifiedinstallment');

Route::get('/unverifiedinstallment','InstallmentController@unverified')->name('unverifiedinstallment');




Route::get('/installmentrcreate/{id}','InstallmentController@create')->name('installmentcreate');
Route::post('/installmentcreate','InstallmentController@store')->name('installmentcreate');
Route::get('/installmentview/{id}','InstallmentController@show')->name('installmentview');

Route::post('/installmentview','InstallmentController@showData')->name('installmentview');

Route::get('/installmentedit/{id}','InstallmentController@edit')->name('installmentedit');
Route::post('/installmentupdate','InstallmentController@update')->name('installmentupdate');
Route::post('/installmentdelete','InstallmentController@destroy')->name('installmentdelete');


/////////////////end coustomer Route//////////////////////////



////////////////Customer Route////////////////////
///



Route::get('/officecontactindex','OfficecontactController@index')->name('officecontactindex');
Route::get('/officecontactcreate/','OfficecontactController@create')->name('officecontactcreate');
Route::post('/officecontactcreate','OfficecontactController@store')->name('officecontactcreate');
Route::get('/officecontactview/{id}','OfficecontactController@show')->name('officecontactview');

Route::get('/officecontactedit/{id}','OfficecontactController@edit')->name('officecontactedit');
Route::post('/officecontactupdate','OfficecontactController@update')->name('officecontactupdate');
Route::post('/officecontactdelete','OfficecontactController@destroy')->name('officecontactdelete');


/////////////////end coustomer Route//////////////////////////


////////////////Commission Route////////////////////
///



Route::get('/commissionindex','CommissionController@index')->name('commissionindex');
Route::get('/commissioncreate/','CommissionController@create')->name('commissioncreate');
Route::post('/commissioncreate','CommissionController@store')->name('commissioncreate');
Route::get('commissiontview/{id}','CommissionController@show')->name('commissionview');

Route::get('/commissionedit/{id}','CommissionController@edit')->name('commissionedit');
Route::post('/commissionupdate','CommissionController@update')->name('commissionupdate');
Route::post('/commissiondelete','CommissionController@destroy')->name('commissiondelete');


/////////////////end Commission Route//////////////////////////






































//Route::get('/test',function (){
//    return view('test.test');
//});

//
////Route::get('/test','Test@index')->name('index');
//Route::get('/create','TestingController@create')->name('create');
//Route::post('/create','TestingController@store')->name('store');
//Route::get('/view','TestingController@index')->name('view');
//Route::get('/edit','TestingController@edit')->name('edit');
//Route::post('/edit','TestingController@update')->name('update');




